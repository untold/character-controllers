using UnityEditor;
using UnityEngine;

namespace CharacterControllers.Editors.Utility {
    internal static class Styles {
        #region Properties
        public static GUISkin Skin { get; } = (GUISkin)EditorGUIUtility.Load("Packages/com.itstheconnection.character-controllers/Editor Default Resources/Styles/CharCons GUISkin.guiskin");

        public static GUIStyle DefaultField { get; } = Skin.FindStyle("default-field");
        public static GUIStyle ValueField { get; } = Skin.FindStyle("value-field");
        #endregion
    }
}