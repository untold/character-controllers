using CharacterControllers.Runtime.Controllers;
using UnityEditor;

namespace CharacterControllers.Editors.CustomEditors {
    [CustomEditor(typeof(DynamicController), true)]
    internal sealed class DynamicControllerCE : Editor {
        public override void OnInspectorGUI() {
            serializedObject.Update();

            #region Props: Settings
            var gravityWeightProp = serializedObject.FindProperty("m_gravityWeight");
            var dragProp = serializedObject.FindProperty("m_drag");
            var maxHorizontalVelocityProp = serializedObject.FindProperty("m_maxHorizontalVelocity");
            var maxUpwardsVelocityProp = serializedObject.FindProperty("m_maxUpwardsVelocity");
            var maxDownwardsVelocityProp = serializedObject.FindProperty("m_maxDownwardsVelocity");

            gravityWeightProp.isExpanded = EditorGUILayout.BeginFoldoutHeaderGroup(gravityWeightProp.isExpanded, "Settings");
            if (gravityWeightProp.isExpanded) {
                EditorGUILayout.PropertyField(gravityWeightProp);
                EditorGUILayout.PropertyField(dragProp);
                EditorGUILayout.Separator();
                EditorGUILayout.PropertyField(maxHorizontalVelocityProp);
                EditorGUILayout.PropertyField(maxUpwardsVelocityProp);
                EditorGUILayout.PropertyField(maxDownwardsVelocityProp);

            }
            EditorGUILayout.EndFoldoutHeaderGroup();
            #endregion

            #region Props: Limits
            var alwaysApplyGravityProp = serializedObject.FindProperty("m_alwaysApplyGravity");
            var keepOnGroundCastDistanceProp = serializedObject.FindProperty("m_keepOnGroundCastDistance");
            var valuesLerpQuicknessProp = serializedObject.FindProperty("m_valuesLerpQuickness");

            maxHorizontalVelocityProp.isExpanded = EditorGUILayout.BeginFoldoutHeaderGroup(maxHorizontalVelocityProp.isExpanded, "Other");
            if (maxHorizontalVelocityProp.isExpanded) {
                EditorGUILayout.PropertyField(keepOnGroundCastDistanceProp);
                EditorGUILayout.PropertyField(valuesLerpQuicknessProp);
                EditorGUILayout.Separator();
                EditorGUILayout.PropertyField(alwaysApplyGravityProp);
            }
            EditorGUILayout.EndFoldoutHeaderGroup();
            #endregion

            #region Specialization
            // valuesLerpQuicknessProp is the last property of DynamicController
            var firstChildProp = valuesLerpQuicknessProp.Copy();
            if (firstChildProp.NextVisible(false)) {
                firstChildProp.isExpanded = EditorGUILayout.BeginFoldoutHeaderGroup(firstChildProp.isExpanded, "Specialization");
                if (firstChildProp.isExpanded) {
                    EditorGUILayout.PropertyField(firstChildProp);
                    while (firstChildProp.NextVisible(false))
                        EditorGUILayout.PropertyField(firstChildProp);
                }
                EditorGUILayout.EndFoldoutHeaderGroup();
            }
            #endregion

            serializedObject.ApplyModifiedProperties();
        }
    }
}