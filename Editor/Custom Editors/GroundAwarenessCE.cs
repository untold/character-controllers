using CharacterControllers.Runtime.PhysicsAwareness;
using UnityEditor;

namespace CharacterControllers.Editors.CustomEditors {
    [CustomEditor(typeof(GroundAwareness), true)]
    public class GroundAwarenessCE : PhysicsAwarenessCE {
        protected override void OnDraw() {
            var mScriptProp = serializedObject.FindProperty("m_Script");
            mScriptProp.isExpanded = EditorGUILayout.BeginFoldoutHeaderGroup(mScriptProp.isExpanded, "Settings");

            if (mScriptProp.isExpanded)
                base.OnDraw();

            EditorGUILayout.EndFoldoutHeaderGroup();
        }
    }
}