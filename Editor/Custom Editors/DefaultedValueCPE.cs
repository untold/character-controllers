﻿using CharacterControllers.Editors.Utility;
using CharacterControllers.Runtime.Utility;
using UnityEditor;
using UnityEngine;

namespace CharacterControllers.Editors.CustomEditors {
    [CustomPropertyDrawer(typeof(DefaultedValue<>))]
    internal sealed class DefaultedValueCPE : PropertyDrawer {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) => EditorGUIUtility.singleLineHeight;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            var indent = EditorGUI.indentLevel * 15f;
            var labelRect = new Rect(position) {
                width = EditorGUIUtility.labelWidth + 2f - indent
            };

            var w = position.width - labelRect.width + indent;
            var leftRect = new Rect(position) {
                x = labelRect.xMax,
                width = Mathf.Max(w * .5f, 26f)
            };
            var rightRect = new Rect(position) {
                x = leftRect.xMax - indent,
                width = Mathf.Max(w * .5f + 1, 26f)
            };

            var defVal = property.FindPropertyRelative("m_defaultValue");
            var val = property.FindPropertyRelative("<Value>k__BackingField");

            EditorGUI.BeginProperty(position, label, property);

            EditorGUI.PrefixLabel(labelRect, label);
            GUI.enabled = !EditorApplication.isPlaying;
            switch (defVal.propertyType) {
                case SerializedPropertyType.Float:
                    EditorGUI.BeginChangeCheck();
                    defVal.floatValue = EditorGUI.FloatField(leftRect, defVal.floatValue, Styles.DefaultField);
                    if (EditorGUI.EndChangeCheck())
                        val.floatValue = defVal.floatValue;
                    break;
                case SerializedPropertyType.Integer:
                    EditorGUI.BeginChangeCheck();
                    defVal.intValue = EditorGUI.IntField(leftRect, defVal.intValue, Styles.DefaultField);
                    if (EditorGUI.EndChangeCheck())
                        val.intValue = defVal.intValue;
                    break;
            }

            GUI.enabled = EditorApplication.isPlaying;
            switch (defVal.propertyType) {
                case SerializedPropertyType.Float:
                    val.floatValue = EditorGUI.FloatField(rightRect, val.floatValue, Styles.ValueField);
                    break;
                case SerializedPropertyType.Integer:
                    val.intValue = EditorGUI.IntField(rightRect, val.intValue, Styles.ValueField);
                    break;
            }

            GUI.enabled = true;

            EditorGUI.EndProperty();
        }
    }
}