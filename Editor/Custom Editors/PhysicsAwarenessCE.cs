using CharacterControllers.Runtime.Core;
using UnityEditor;
using UnityEngine;

namespace CharacterControllers.Editors.CustomEditors {
    [CustomEditor(typeof(PhysicsAwarenessBase), true)]
    public class PhysicsAwarenessCE : Editor {
        public override void OnInspectorGUI() {
            serializedObject.Update();

            var forceResultProp = serializedObject.FindProperty("m_forceResult");
            var forcedResultValueProp = serializedObject.FindProperty("m_forcedResult");

            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.PropertyField(forceResultProp, GUILayout.MaxWidth(EditorGUIUtility.labelWidth + 20f));

                if (forceResultProp.boolValue) {
                    var label = forcedResultValueProp.boolValue ? "True" : "False";
                    forcedResultValueProp.boolValue = GUILayout.Toggle(forcedResultValueProp.boolValue, label, EditorStyles.miniButton);
                }
            }
            EditorGUILayout.EndHorizontal();

            if (GetType() == typeof(PhysicsAwarenessCE))
                EditorGUILayout.Separator();

            OnDraw();

            serializedObject.ApplyModifiedProperties();
        }

        protected virtual void OnDraw() {
            DrawPropertiesExcluding(serializedObject, "m_Script", "m_forceResult", "m_forcedResult");
        }
    }
}