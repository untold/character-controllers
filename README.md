# About Character Controllers

Use ItsTheConnection's Character Controllers to have a solid common base for different character controllers.

# Installing Character Controllers

To install this package, follow the instructions in the [Package Manager documentation](https://docs.unity3d.com/Packages/com.unity.package-manager-ui@latest/index.html).

# Using Character Controllers

The Character Controllers Manual can be found [here](https://docs.google.com/document/d/1d6iILltewDgMhMWFHvtlxmzLFOZaFzeNpHs4BrjDc9E/edit?usp=sharing).


# Technical details
## Requirements

This version of Character Controllers is compatible with the following versions of the Unity Editor:

* 2022.1 and later (recommended)

## Package contents

The following table indicates the folder structure of the Sprite package:

|Location|Description|
|---|---|
|`Editor`|Root folder containing the source for the Character Controllers.|
|`Editor Default Resources`|Root folder containing the graphical Assets for the package.|
|`Runtime`|Root folder containing the source for the runtime of the Character Controllers.|

## Document revision history

|Date|Reason|
|---|---|
|May 28th, 2023|Document created. Matches package version 0.0.1|