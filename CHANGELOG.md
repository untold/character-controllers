# Changelog
All notable changes to this package will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)

---

## [0.0.1] - 2023-05-28
### This is the first release of Character Controllers, as a Package.