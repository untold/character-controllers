using System.Reflection;
using CharacterControllers.Runtime.Utility;
using UnityEditor;
using UnityEngine;

namespace CharacterControllers.Runtime.Core {
    public record GroundInfo {
        public Vector2 Point { get; set; }
        public Vector2 Normal { get; set; }
        public Vector2 Tangent { get; set; }
        public float Distance { get; set; }
        public bool DidHit { get; set; }
    }

    [RequireComponent(typeof(Rigidbody2D))]
    [Icon("Packages/com.itstheconnection.character-controllers/Editor Default Resources/Script Icons/Physics2DController - Icon.png")]
    public abstract class Physics2DControllerBase : MonoBehaviour {
        #region Public Variables
#if UNITY_EDITOR
        [Header("Debug")]
        public bool drawVectors = true;
#endif
        [field: SerializeField] public bool DoApplyPhysics { get; set; } = true;

        [Header("Components")]
        [SerializeField] private Rigidbody2D m_rigidbody;

        [Header("Settings")]
        [SerializeField] private DefaultedValue<float> m_gravityWeight = 1f;
        [SerializeField] private DefaultedValue<float> m_maxFallSpeed = 16f;
        [SerializeField] private DefaultedValue<float> m_airResistance = 18f;
        [SerializeField] private float m_coyoteTime = .1f;
        [SerializeField] private float m_coyoteTimeWallGrab = .1f;

        [Space]
        [SerializeField] private DefaultedValue<float> m_gravityLerpQuickness = 1f;
        [SerializeField] private DefaultedValue<float> m_maxFallSpeedLerpQuickness = 1f;

        [Space]
        [SerializeField] private bool m_groundStatus;
        [SerializeField] private bool m_wallStatus;

        public event System.Action<bool> OnGroundedStatusChanged, OnWallStatusChanged;
        #endregion

        #region Private Variables
#if UNITY_EDITOR
        private const BindingFlags k_flags = BindingFlags.Default | BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy;
#endif

        private float _coyoteTimeEnd, _coyoteTimeWallGrabEnd;
        #endregion

        #region Properties
        protected LayerMask AllButSelfMask { get; private set; }

        public Rigidbody2D Body => m_rigidbody;
#if UNITY_6000
        public Vector2 Velocity {
            get => Body.linearVelocity;
            protected set => Body.linearVelocity = value;
        }
#else
        public Vector2 Velocity {
            get => Body.velocity;
            protected set => Body.velocity = value;
        }
#endif
        public Vector2 LocalVelocity { get; private set; }

        public Vector2 GroundPoint { get; protected set; }
        public Vector2 GroundNormal { get; protected set; } = Vector3.up;
        public Vector2 GroundTangent { get; protected set; } = Vector3.right;
        public float GroundDistance { get; protected set; }
        public float GroundAngle { get; protected set; }

        public bool IsGrounded {
            get => m_groundStatus;
            private set {
                if (value == m_groundStatus)
                    return;

                m_groundStatus = value;
                OnGroundedStatusChangedCallback(m_groundStatus);
            }
        }
        public bool IsCoyoteTime => Time.time < _coyoteTimeEnd;
        public float LastLandTime { get; private set; }

        public Vector2 WallPoint { get; protected set; }
        public Vector2 WallNormal { get; protected set; } = Vector3.left;
        public Vector2 WallTangent { get; protected set; } = Vector3.up;
        public float WallDistance { get; protected set; }
        public float WallAngle { get; protected set; }

        public bool IsWalled {
            get => m_wallStatus;
            private set {
                if (value == m_wallStatus)
                    return;

                m_wallStatus = value;
                OnWallStatusChangedCallback(m_wallStatus);
            }
        }
        public bool IsCoyoteTimeWallGrab => Time.time < _coyoteTimeWallGrabEnd;
        public float LastWallTime { get; private set; }

        public float GravityWeight { get; private set; }
        public float MaxFallSpeed { get; private set; }

        public bool Simulated {
            get => m_rigidbody.simulated;
            set => m_rigidbody.simulated = value;
        }

        protected Vector2 Inertia { get; private set; }

        private float SmoothingGravityWeight => m_gravityLerpQuickness;
        private float SmoothingMaxFallSpeed => m_maxFallSpeedLerpQuickness;
        #endregion

        #region Behaviour Callbacks
        private void Reset() => OnValidate();

        protected virtual void OnValidate() {
            m_rigidbody = GetComponent<Rigidbody2D>();
            m_rigidbody.gravityScale = 0f;
        }

        private void Awake() {
            SetUp();
        }

        private void Start() {
            OnLateSetUp();
        }

        protected virtual void Update() {
            GravityWeight = Mathf.Lerp(GravityWeight, m_gravityWeight, Time.deltaTime * SmoothingGravityWeight);
            MaxFallSpeed = Mathf.Lerp(MaxFallSpeed, m_maxFallSpeed, Time.deltaTime * SmoothingMaxFallSpeed);
        }

        private void FixedUpdate() {
            var groundInfo = OnGroundCheck();
            GroundPoint = groundInfo.Point;
            GroundNormal = groundInfo.Normal;
            GroundTangent = groundInfo.Tangent;
            GroundDistance = groundInfo.Distance;
            GroundAngle = Vector2.SignedAngle(Vector2.up, GroundNormal);
            IsGrounded = groundInfo.DidHit;

            var wallInfo = OnWallCheck();
            WallPoint = wallInfo.Point;
            WallNormal = wallInfo.Normal;
            WallTangent = wallInfo.Tangent;
            WallDistance = wallInfo.Distance;
            WallAngle = Vector2.SignedAngle(Vector2.right * Mathf.Sign(WallNormal.x), WallNormal);
            IsWalled = wallInfo.DidHit;

            Inertia = Inertia.sqrMagnitude > .01f ? InertialVelocityDrag(Inertia) : Vector2.zero;

            LocalVelocity = transform.InverseTransformVector(Velocity);

            if (DoApplyPhysics)
                ApplyPhysics();

            if (IsGrounded)
                return;

            if (Mathf.Abs(GravityWeight) < .01f)
                return;

            if (Mathf.Abs(MaxFallSpeed) < .01f)
                return;

            if (Velocity.y > -MaxFallSpeed)
                Body.AddForce(Physics.gravity.magnitude * m_rigidbody.mass * GravityWeight * -GroundNormal, ForceMode2D.Force);
            else if (Velocity.y < -MaxFallSpeed - 0.5f)
                Body.AddForce(m_airResistance * m_rigidbody.mass * Vector2.up, ForceMode2D.Force);
        }

        protected virtual void OnDrawGizmos() {
            if (!enabled)
                return;

            if (!drawVectors)
                return;

            #region Draw Orientation Vectors
            Gizmos.color = Color.cyan;
            Gizmos.DrawRay(transform.position, GroundNormal);
            Gizmos.color = Color.magenta;
            Gizmos.DrawRay(transform.position, GroundTangent);
            Gizmos.color = Color.white;
            #endregion

#if UNITY_EDITOR
            if (EditorApplication.isPlaying) {
                Gizmos.color = Color.black;
                Gizmos.DrawSphere(GroundPoint, .01f);
                Gizmos.color = Color.white;
            }
#endif

            #region Draw Grounded Gizmos
            var onGroundCheckMethod = GetType().GetMethod("OnGroundCheck", k_flags)!;
            if (onGroundCheckMethod.DeclaringType == typeof(Physics2DControllerBase)) {
                Gizmos.color = IsGrounded ? Color.green : Color.red;
                Gizmos.DrawRay(transform.position + transform.up * .1f, Vector2.down * .25f);
                Gizmos.color = Color.white;
            }
            #endregion

            #region Draw Walled Gizmos
            var onWallCheckMethod = GetType().GetMethod("OnWallCheck", k_flags)!;
            if (onWallCheckMethod.DeclaringType == typeof(Physics2DControllerBase)) {
                Gizmos.color = IsWalled ? Color.green : Color.red;
                Gizmos.DrawRay(transform.position + transform.up * .1f, Vector2.right * .25f);
                Gizmos.DrawRay(transform.position + transform.up * .1f, Vector2.left * .25f);
                Gizmos.color = Color.white;
            }
            #endregion
        }
        #endregion

        #region Public Methods
        public void AddInertialVelocity(Vector2 velocity) => Inertia = velocity;
        public void ClearInertia() => Inertia = Vector2.zero;

        public void SetGravityWeight(float weight) => m_gravityWeight.Value = weight;
        public void SetGravityWeightSudden(float weight) => GravityWeight = m_gravityWeight.Value = weight;
        public void ResetGravityWeight() => m_gravityWeight.Reset();
        public void ResetGravityWeightSudden() => GravityWeight = m_gravityWeight.Reset();

        public void SetMaxFallSpeed(float maxFallSpeed) => m_maxFallSpeed.Value = maxFallSpeed;
        public void SetMaxFallSpeedSudden(float maxFallSpeed) => MaxFallSpeed = m_maxFallSpeed.Value = maxFallSpeed;
        public void ResetMaxFallSpeed() => m_maxFallSpeed.Reset();
        public void ResetMaxFallSpeedSudden() => MaxFallSpeed = m_maxFallSpeed.Reset();

        public void SetSmoothing(SmoothingType smoothingType, float value) {
            if (smoothingType.HasFlag(SmoothingType.GravityWeight))
                m_gravityLerpQuickness.Value = value;
            if (smoothingType.HasFlag(SmoothingType.MaxFallSpeed))
                m_maxFallSpeedLerpQuickness.Value = value;
        }

        public void ResetSmoothing(SmoothingType smoothingType) {
            if (smoothingType.HasFlag(SmoothingType.GravityWeight))
                m_gravityLerpQuickness.Reset();
            if (smoothingType.HasFlag(SmoothingType.MaxFallSpeed))
                m_maxFallSpeedLerpQuickness.Reset();
        }

        public void CancelCoyoteTime() => _coyoteTimeEnd = Time.time;

        public virtual Vector3 Reorient(in Vector3 input) => transform.TransformDirection(input);

        public void Shutdown() {
            OnShutdown();

            enabled = false;
        }
        #endregion

        #region Private Methods
        private void SetUp() {
            AllButSelfMask = ~(1 << gameObject.layer);

            ResetGravityWeightSudden();
            ResetMaxFallSpeedSudden();

            ResetSmoothing(SmoothingType.All);

            OnSetUp();
        }

        protected virtual void OnSetUp() {}
        protected virtual void OnLateSetUp() {}

        protected virtual GroundInfo OnGroundCheck() {
            var ray = new Ray2D {
                origin = transform.position + transform.up * .1f,
                direction = Vector2.down
            };

            var hit = Physics2D.Raycast(ray.origin, ray.direction, .25f, AllButSelfMask);
            if (!hit)
                return new GroundInfo {
                    Normal = Vector2.up,
                    Tangent = Vector2.right,
                    Point = transform.position,
                    Distance = 0f,
                    DidHit = false
                };

            return new GroundInfo {
                Normal = hit.normal,
                Tangent = -Vector2.Perpendicular(hit.normal),
                Point = hit.point,
                Distance = hit.distance,
                DidHit = true
            };
        }

        protected virtual GroundInfo OnWallCheck() {
            var ray = new Ray2D {
                origin = transform.position + transform.up * .1f,
                direction = Vector2.right
            };

            var hit = Physics2D.Raycast(ray.origin, ray.direction, .25f, AllButSelfMask);
            if (hit)
                return new GroundInfo {
                    Normal = hit.normal,
                    Tangent = -Vector2.Perpendicular(hit.normal),
                    Point = hit.point,
                    Distance = hit.distance,
                    DidHit = true
                };

            ray.direction = Vector2.left;
            hit = Physics2D.Raycast(ray.origin, ray.direction, .25f, AllButSelfMask);
            if (hit)
                return new GroundInfo {
                    Normal = hit.normal,
                    Tangent = -Vector2.Perpendicular(hit.normal),
                    Point = hit.point,
                    Distance = hit.distance,
                    DidHit = true
                };

            return new GroundInfo {
                Normal = Vector2.left,
                Tangent = Vector2.up,
                Point = transform.position,
                Distance = 0f,
                DidHit = false
            };
        }

        protected abstract void ApplyPhysics();

        protected virtual void OnShutdown() { }

        private void OnGroundedStatusChangedCallback(bool newValue) {
            if (newValue) {
                _coyoteTimeEnd = 0;
                LastLandTime = Time.time;
            } else
                _coyoteTimeEnd = Time.time + m_coyoteTime;
            OnGroundedStatusChanged?.Invoke(newValue);
        }

        private void OnWallStatusChangedCallback(bool newValue) {
            if (newValue) {
                _coyoteTimeWallGrabEnd = 0;
                LastWallTime = Time.time;
            }
            else
                _coyoteTimeWallGrabEnd = Time.time + m_coyoteTimeWallGrab;
            OnWallStatusChanged?.Invoke(newValue);
        }

        private Vector2 InertialVelocityDrag(Vector2 velocity) {
            velocity = PhysicsUtils.ApplyDragSmart(velocity, m_airResistance);
            Debug.DrawRay(transform.position, velocity, Color.cyan, 0.02f);
            return velocity;
        }
        #endregion

        #region Event Methods
        #endregion
    }
}