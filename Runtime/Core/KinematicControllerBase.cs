﻿using UnityEngine;

namespace CharacterControllers.Runtime.Core {
    [RequireComponent(typeof(CapsuleCollider))]
    [RequireComponent(typeof(Rigidbody))]
    [Icon("Packages/com.itstheconnection.character-controllers/Editor Default Resources/Script Icons/KinematicController - Icon.png")]
    public class KinematicControllerBase : MonoBehaviour {
        #region Public Variables
        [Header("Debug")]
        [SerializeField] private bool drawVectors;

        [Header("References")]
        [SerializeField] private Rigidbody m_rigidbody;

        [Header("Thresholds")]
        [SerializeField, Min(0.0001f)] private float m_stillThreshold = .01f;

        [Header("Ground Check")]
        [SerializeField] private float m_castDistance = .005f;
        [SerializeField] private float m_coyoteTime = .1f;

        [Space]
        [SerializeField, Range(0, 90f)] protected float m_slopeThreshold = 45f;

        [Header("Runtime")]
        [SerializeField] private bool m_groundStatus;
        [SerializeField] private bool m_slopeStatus;

        public event System.Action<bool> OnGroundedStatusChanged;
        #endregion

        #region Private Variables
        private bool _wasGrounded;

        private float _coyoteTimeEnd;
        private bool _wasCoyoteTime;
        #endregion

        #region Properties
        public Rigidbody Body => m_rigidbody;

        public bool IsGrounded {
            get => m_groundStatus;
            private set {
                if (value == m_groundStatus)
                    return;

                m_groundStatus = value;
                OnGroundedStatusChangedCallback(m_groundStatus);
            }
        }
        public bool IsCoyoteTime => Time.time < _coyoteTimeEnd;
        public float LastLandTime { get; private set; }

        public bool IsOnSlope { get; private set; }

        public Vector3 Velocity { get; protected set; }
        public Vector3 GroundNormal { get; protected set; }
        public Vector3 GroundTangent { get; protected set; }
        public Vector3 GroundPoint { get; protected set; }

        public float SlopeAngle { get; private set; }
        #endregion

        #region Behaviour Callbacks
        protected virtual void Reset() {
            m_rigidbody ??= GetComponent<Rigidbody>();
            m_rigidbody.useGravity = false;
        }

        protected virtual void OnValidate() {
            m_rigidbody ??= GetComponent<Rigidbody>();
            m_rigidbody.useGravity = false;
            m_rigidbody.isKinematic = true;
        }

        private void Awake() => m_rigidbody = GetComponent<Rigidbody>();

        private void FixedUpdate() {
            CheckGround();
            OnApplyPhysics();
        }

        protected virtual void OnDrawGizmos() {
            if (!drawVectors)
                return;

            var originalColor = Gizmos.color;

            Gizmos.color = Color.magenta;
            Gizmos.DrawRay(GroundPoint, GroundNormal);
            Gizmos.color = Color.red;
            Gizmos.DrawRay(GroundPoint, GroundTangent);
            Gizmos.color = originalColor;
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        private void CheckGround() {
            IsGrounded = OnGroundCheck(out var hit);

            IsOnSlope = hit.normal.y < Mathf.Sin(m_slopeThreshold * Mathf.Deg2Rad);
            SlopeAngle = IsOnSlope
                ? Vector3.SignedAngle(Vector3.up, GroundNormal, Vector3.back)
                : 0f;

            if (IsGrounded == _wasGrounded)
                return;

            OnGroundedStatusChanged?.Invoke(IsGrounded);
            _wasGrounded = IsGrounded;
        }

        protected virtual bool OnGroundCheck(out RaycastHit hit) {
            Body.SweepTest(Vector3.down, out hit, m_castDistance, QueryTriggerInteraction.Ignore);

            GroundNormal = hit.normal;
            GroundTangent = Quaternion.Euler(0, 0, -90) * GroundNormal;
            GroundPoint = hit.point;
            IsOnSlope = hit.normal.y < Mathf.Sin(m_slopeThreshold * Mathf.Deg2Rad);
            return hit.collider;
        }

        protected virtual void OnApplyPhysics() {}

        private void OnGroundedStatusChangedCallback(bool newValue) {
            if (newValue) {
                _coyoteTimeEnd = 0;
                LastLandTime = Time.time;
            } else
                _coyoteTimeEnd = Time.time + m_coyoteTime;
            OnGroundedStatusChanged?.Invoke(newValue);
        }
        #endregion
    }
}