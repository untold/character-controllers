using JetBrains.Annotations;
using UnityEngine;

namespace CharacterControllers.Runtime.Core {
    [PublicAPI]
    public enum CollisionShape {
        Line,
        Sphere,
        Capsule,
        Box
    }

    public delegate void AwarenessStateChangeEventHandler(bool isAware);

    [PublicAPI]
    public abstract class PhysicsAwarenessBase : MonoBehaviour {
        #region Public Variables
        [SerializeField] private bool m_forceResult;
        [SerializeField] private bool m_forcedResult = true;
        [SerializeField, Min(0f)] private float m_coyoteTime = 0.1f;

        public event AwarenessStateChangeEventHandler OnAwarenessStateChange;
        public event System.Action OnCoyoteTimeEnded;
        #endregion

        #region Private Variables
        private bool _wasAware;
        private float _coyoteEndTime;
        #endregion

        #region Properties
        public bool IsAware { get; private set; }

        public bool IsCoyoteAware { get; private set; }
        #endregion

        #region Behaviour Callbacks
        private void FixedUpdate() {
            IsAware = m_forceResult ? m_forcedResult : OnAwarenessCheck();

            if (IsAware != _wasAware) {
                if (!IsAware) {
                    _coyoteEndTime = Time.time + m_coyoteTime;
                    IsCoyoteAware = true;
                }

                OnAwarenessStateChange?.Invoke(IsAware);
            }

            if (IsCoyoteAware && Time.time >= _coyoteEndTime) {
                IsCoyoteAware = false;
                OnCoyoteTimeEnded?.Invoke();
            }

            _wasAware = IsAware;
        }
        #endregion

        #region Public Methods
        public void ClearCoyoteTime() {
            _coyoteEndTime = -1;
            IsCoyoteAware = false;
        }
        #endregion

        #region Virtual Methods
        protected abstract bool OnAwarenessCheck();
        #endregion
    }
}