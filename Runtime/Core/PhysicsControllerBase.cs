using CharacterControllers.Runtime.Utility;
using UnityEngine;

namespace CharacterControllers.Runtime.Core {
    [System.Flags] public enum SmoothingType { None = 0, GravityWeight = 1, MaxFallSpeed = 2, All = ~0 }

    [RequireComponent(typeof(Rigidbody))]
    [Icon("Packages/com.itstheconnection.character-controllers/Editor Default Resources/Script Icons/PhysicsController - Icon.png")]
    public abstract class PhysicsControllerBase : MonoBehaviour {
        #region Public Variables
        [Header("Debug")]
        [SerializeField] private bool drawVectors;

        [Header("Components")]
        [SerializeField] private Rigidbody m_rigidbody;

        [Header("Settings")]
        [SerializeField] private DefaultedValue<float> m_gravityWeight = 1f;
        [SerializeField] private DefaultedValue<float> m_maxFallSpeed = 1f;
        [SerializeField] private float m_airResistance = 18f;
        [SerializeField] private float m_coyoteTime = .1f;
        [SerializeField] private float m_coyoteTimeWallGrab = .1f;
        [SerializeField, Range(0, 90f)] private float m_slopeLimit = 45f;
        [Space(2)]
        [SerializeField] private DefaultedValue<float> m_gravityLerpQuickness = 1f;
        [SerializeField] private DefaultedValue<float> m_maxFallSpeedLerpQuickness = 1f;

        [SerializeField] private bool m_groundStatus;
        [SerializeField] private bool m_wallStatus;
        [SerializeField] private bool m_slopeStatus;

        public event System.Action OnCoyoteTimeEnded, OnCoyoteTimeWallGrabEnded;
        public event System.Action<bool> OnGroundedStatusChanged, OnWallStatusChanged, OnSlopeStatusChanged;
        #endregion

        #region Private Variables
        [field: SerializeField] public bool DontApplyPhysics { get; set; }

        private float _coyoteTimeEnd, _coyoteTimeSlopeEnd, _coyoteTimeWallGrabEnd;
        private bool _wasCoyoteTime, _wasCoyoteTimeWallGrab;
        #endregion

        #region Properties
        public Rigidbody Body => m_rigidbody;
#if UNITY_6000
        public Vector3 Velocity {
            get => Body.linearVelocity;
            protected set => Body.linearVelocity = value;
        }
#else
        public Vector3 Velocity {
            get => Body.velocity;
            protected set => Body.velocity = value;
        }
#endif

        public Vector3 LocalVelocity { get; private set; }

        protected float GravityWeight { get; private set; }
        protected float MaxFallSpeed { get; private set; }
        protected Vector3 Inertia { get; private set; }

        private float SmoothingGravityWeight => m_gravityLerpQuickness;
        private float SmoothingMaxFallSpeed => m_maxFallSpeedLerpQuickness;

        public Vector3 GroundNormal { get; protected set; } = Vector3.up;
        public Vector3 GroundTangent { get; protected set; } = Vector3.right;
        public float SlopeAngle { get; protected set; }

        public bool IsSloped {
            get => m_slopeStatus;
            private set {
                if (value == m_slopeStatus)
                    return;

                m_slopeStatus = value;
                OnSlopedStatusChangedCallback(m_slopeStatus);
            }
        }
        public bool IsCoyoteTimeSlope => Time.time < _coyoteTimeSlopeEnd;
        public float LastSlopeTime { get; private set; }

        public bool IsGrounded {
            get => m_groundStatus;
            private set {
                if (value == m_groundStatus)
                    return;

                m_groundStatus = value;
                OnGroundedStatusChangedCallback(m_groundStatus);
            }
        }
        public bool IsCoyoteTime => Time.time < _coyoteTimeEnd;
        public float LastLandTime { get; private set; }

        public bool IsWalled {
            get => m_wallStatus;
            private set {
                if (value == m_wallStatus)
                    return;

                m_wallStatus = value;
                OnWallStatusChangedCallback(m_wallStatus);
            }
        }
        public bool IsCoyoteTimeWallGrab => Time.time < _coyoteTimeWallGrabEnd;
        public float LastWallTime { get; private set; }
        #endregion

        #region Behaviour Callbacks
        protected virtual void Reset() {
            m_rigidbody ??= GetComponent<Rigidbody>();
            m_rigidbody.useGravity = false;
        }

        protected virtual void OnValidate() {
            m_rigidbody ??= GetComponent<Rigidbody>();
            m_rigidbody.useGravity = false;
        }

        private void Awake() {
            SetUp();
        }

        private void Start() {
            OnLateSetUp();
        }

        protected virtual void Update() {
            GravityWeight = Mathf.Lerp(GravityWeight, m_gravityWeight, Time.deltaTime * SmoothingGravityWeight);
            MaxFallSpeed = Mathf.Lerp(MaxFallSpeed, m_maxFallSpeed, Time.deltaTime * SmoothingMaxFallSpeed);

            if (_wasCoyoteTime && !IsCoyoteTime)
                OnCoyoteTimeEnded?.Invoke();
            _wasCoyoteTime = IsCoyoteTime;

            if (_wasCoyoteTimeWallGrab && !IsCoyoteTimeWallGrab)
                OnCoyoteTimeWallGrabEnded?.Invoke();
            _wasCoyoteTimeWallGrab = IsCoyoteTimeWallGrab;
        }

        private void FixedUpdate() {
            IsGrounded = CheckForGround();
            IsWalled = CheckForWalls();

            SlopeAngle = Vector3.Angle(Vector3.up, GroundNormal);
            IsSloped = SlopeAngle > m_slopeLimit;

            Inertia = Inertia.sqrMagnitude > .01f ? InertialVelocityDrag(Inertia) : Vector3.zero;

            LocalVelocity = transform.InverseTransformVector(Velocity);

            if (!DontApplyPhysics)
                ApplyPhysics();

            if (IsGrounded)
                return;

            if (Mathf.Abs(GravityWeight) < .01f)
                return;

            if (Velocity.y > -MaxFallSpeed)
                Body.AddForce(Physics.gravity.magnitude * GravityWeight * -GroundNormal, ForceMode.Acceleration);
            else if (Velocity.y < -MaxFallSpeed - 0.5f)
                Body.AddForce(Vector3.up * m_airResistance, ForceMode.Acceleration);
        }

        protected virtual void OnDrawGizmos() {
            if (!drawVectors)
                return;

            var originalColor = Gizmos.color;

            Gizmos.color = Color.magenta;
            Gizmos.DrawRay(Body.position, GroundNormal);
            Gizmos.color = Color.red;
            Gizmos.DrawRay(Body.position, GroundTangent);
            Gizmos.color = originalColor;
        }
        #endregion

        #region Public Functions
        public void AddInertialVelocity(Vector3 velocity) => Inertia = velocity;

        public void SetGravityWeight(float weight) => m_gravityWeight.Value = weight;
        public void SetGravityWeightSudden(float weight) => GravityWeight = m_gravityWeight.Value = weight;
        public void ResetGravityWeight() => m_gravityWeight.Reset();
        public void ResetGravityWeightSudden() => GravityWeight = m_gravityWeight.Reset();

        public void SetMaxFallSpeed(float maxFallSpeed) => m_maxFallSpeed.Value = maxFallSpeed;
        public void SetMaxFallSpeedSudden(float maxFallSpeed) => MaxFallSpeed = m_maxFallSpeed.Value = maxFallSpeed;
        public void ResetMaxFallSpeed() => m_maxFallSpeed.Reset();
        public void ResetMaxFallSpeedSudden() => MaxFallSpeed = m_maxFallSpeed.Reset();

        public void SetSmoothing(SmoothingType smoothingType, float value) {
            if (smoothingType.HasFlag(SmoothingType.GravityWeight))
                m_gravityLerpQuickness.Value = value;
            if (smoothingType.HasFlag(SmoothingType.MaxFallSpeed))
                m_maxFallSpeedLerpQuickness.Value = value;
        }

        public void ResetSmoothing(SmoothingType smoothingType) {
            if (smoothingType.HasFlag(SmoothingType.GravityWeight))
                m_gravityLerpQuickness.Reset();
            if (smoothingType.HasFlag(SmoothingType.MaxFallSpeed))
                m_maxFallSpeedLerpQuickness.Reset();
        }

        public void CancelCoyoteTime() => _coyoteTimeEnd = Time.time;
        public void CancelCoyoteTimeWallGrab() => _coyoteTimeWallGrabEnd = Time.time;

        public virtual Vector3 Reorient(in Vector3 input) => transform.TransformDirection(input);
        #endregion

        #region Private Methods
        private void SetUp() {
            ResetGravityWeightSudden();
            ResetMaxFallSpeedSudden();

            ResetSmoothing(SmoothingType.All);

            OnSetUp();
        }

        private void OnSlopedStatusChangedCallback(bool newValue) {
            if (newValue) {
                _coyoteTimeSlopeEnd = 0;
                LastSlopeTime = Time.time;
            } else
                _coyoteTimeSlopeEnd = Time.time + m_coyoteTime;
            OnSlopeStatusChanged?.Invoke(newValue);
        }

        private void OnGroundedStatusChangedCallback(bool newValue) {
            if (newValue) {
                _coyoteTimeEnd = 0;
                LastLandTime = Time.time;
            } else
                _coyoteTimeEnd = Time.time + m_coyoteTime;
            OnGroundedStatusChanged?.Invoke(newValue);
        }

        private void OnWallStatusChangedCallback(bool newValue) {
            if (newValue) {
                _coyoteTimeWallGrabEnd = 0;
                LastWallTime = Time.time;
            } else
                _coyoteTimeWallGrabEnd = Time.time + m_coyoteTimeWallGrab;
            OnWallStatusChanged?.Invoke(newValue);
        }

        private Vector3 InertialVelocityDrag(Vector3 velocity) {
            velocity = PhysicsUtils.ApplyDragSmart(velocity, m_airResistance);
            Debug.DrawRay(transform.position, velocity, Color.cyan, 0.02f);
            return velocity;
        }
        #endregion

        #region Overrideable Methods
        protected virtual void OnSetUp() {}
        protected virtual void OnLateSetUp() {}
        protected abstract bool CheckForGround();
        protected abstract bool CheckForWalls();
        protected abstract void ApplyPhysics();
        #endregion
    }
}