﻿using CharacterControllers.Runtime.Utility;
using UnityEngine;

namespace CharacterControllers.Runtime.Components.TitanFall {
    public sealed class HeadControl : MonoBehaviour {
        #region Public Variables
        [SerializeField] private float m_tiltAngle = 10f;
        [SerializeField] private float m_tiltLerpQuickness = 6;
        [SerializeField] private Camera m_camera;
        [SerializeField] private DefaultedValue<float> m_fov = 80f;
        [SerializeField] private float m_fovLerpQuickness = 10;
        #endregion

        #region Private Variables
        private float _targetTilt;
        #endregion

        #region Behaviour Callbacks
        private void Awake() => m_camera.fieldOfView = m_fov;

        private void Update() {
            transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.Euler(0, 0, _targetTilt), Time.deltaTime * m_tiltLerpQuickness);
            m_camera.fieldOfView = Mathf.Lerp(m_camera.fieldOfView, m_fov, Time.deltaTime * m_fovLerpQuickness);
        }
        #endregion

        #region Public Methods
        public void SetTilt(int side) => _targetTilt = m_tiltAngle * side;
        public void SetFov(float fov) => m_fov.Value = fov;
        public void ResetFov() => m_fov.Reset();
        #endregion
    }
}