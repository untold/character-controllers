﻿using CharacterControllers.Runtime.Core;
using CharacterControllers.Runtime.Utility;
using UnityEditor;
using UnityEngine;

namespace CharacterControllers.Runtime.Components {
    [AddComponentMenu("Character Controllers/TitanFall Controller (Physics)")]
    [RequireComponent(typeof(CapsuleCollider))]
    public class TitanFallPhysicsController : PhysicsControllerBase {
        #region Public Variables
        [Header("Components")]
        [SerializeField] private CapsuleCollider m_collider;
        [SerializeField] private Transform m_head;
        [SerializeField] private Transform m_orientation;

        [Header("Settings")]
        [SerializeField] private DefaultedValue<float> m_moveSpeed = 6f;
        [SerializeField] private float m_airborneResponsiveness = .5f;
        [SerializeField] private float m_sensitivityX = 1f, m_sensitivityY = 1f;
        [SerializeField] private DefaultedValue<float> m_responsiveness = 1f;
        [SerializeField] private DefaultedValue<float> m_drag = 6f;
        [SerializeField] private DefaultedValue<float> m_height = 1.8f;
        [Space(2)]
        [SerializeField] private DefaultedValue<float> m_moveSpeedLerpQuickness = 12;
        [SerializeField] private DefaultedValue<float> m_dragLerpQuickness = 12;
        [SerializeField] private float m_heightLerpQuickness = 8;

        [Header("Ground Casting")]
        [SerializeField] private float m_groundCastDistance = .15f;
        [SerializeField] private float m_groundSlopeLimit = 45f;
        [SerializeField] private LayerMask m_groundMask = 1;

        [Header("Walls Casting")]
        [SerializeField] private float m_wallsCastDistance = .15f;
        [SerializeField] private LayerMask m_wallsMask = 1;
        #endregion

        #region Private Variables
        private float _smoothingVelocityControl = 1f;
        private int _lastNonZeroInputX = 1;

        private float _angleX;
        private float _angleY;
        private Vector3 _moveDirection;

        private bool _moveAlongPlane;
        private Vector3 _moveAlongPlaneNormal;
        #endregion

        #region Properties
        public CapsuleCollider Collider => m_collider;
        public Transform Head => m_head;

        public float MoveSpeed { get; private set; }
        public float Drag { get; private set; }
        public float Height { get; private set; }
        public bool IsAiring { get; set; }

        protected float SmoothingMoveSpeed => m_moveSpeedLerpQuickness;
        protected float SmoothingVelocityControl => _smoothingVelocityControl;

        public Vector3 Direction { get; private set; }
        public Vector2 LookDirection { get; private set; }

        public float SpeedPercent { get; private set; }

        public Vector3 WallNormal { get; private set; }
        public Vector3 WallTangent { get; private set; }
        public int WallSide { get; private set; }
        public Rigidbody GroundBody { get; private set; }
        #endregion

        #region Behaviour Callbacks
        protected override void OnValidate() {
            base.OnValidate();

            m_collider ??= GetComponent<CapsuleCollider>();
        }

        protected override void Update() {
            base.Update();

            MoveSpeed = Mathf.Lerp(MoveSpeed, m_moveSpeed, Time.deltaTime * m_moveSpeedLerpQuickness);
            Drag = Mathf.Lerp(Drag, m_drag, Time.deltaTime * m_dragLerpQuickness);
            Height = Mathf.Lerp(Height, m_height, Time.deltaTime * m_heightLerpQuickness);

            m_head.rotation = Quaternion.Euler(_angleX, _angleY, 0f);
            m_orientation.rotation = Quaternion.Euler(0f, _angleY, 0f);
        }

        protected override void OnDrawGizmos() {
            base.OnDrawGizmos();

            DrawWireCapsule(transform.position + Collider.center, transform.rotation, Collider.radius, Collider.height, Color.yellow);

            var originalColor = Gizmos.color;
            var localUp = transform.up;

            // Ground casting ray
            Gizmos.color = IsGrounded ? Color.green : IsCoyoteTime ? Color.yellow : Color.red;
            Gizmos.DrawRay(Body.position, -localUp * (Collider.height * .5f + m_groundCastDistance));

            Gizmos.color = Color.white;
            Gizmos.DrawRay(Body.position -localUp * Collider.height * .5f, _moveDirection);

            // Ground normal and tangent
            Gizmos.color = Color.magenta;
            Gizmos.DrawRay(Body.position - transform.up * Collider.height * .5f, GroundNormal * .2f);
            Gizmos.color = Color.blue;
            Gizmos.DrawRay(Body.position - transform.up * Collider.height * .5f, GroundTangent * .2f);

            // Wall casting rays
            Gizmos.color = IsWalled ? Color.green : Color.red;
            Gizmos.DrawRay(Body.position, m_orientation.right * (m_collider.radius + m_wallsCastDistance));
            Gizmos.DrawRay(Body.position, -m_orientation.right * (m_collider.radius + m_wallsCastDistance));

            Gizmos.color = originalColor;
        }
        #endregion

        #region Overrides
        protected override void OnSetUp() {
            ResetMaxMoveSpeedSudden();
            ResetHeight();
            SetCursor(false);

            ResetSmoothing(PlatformerSmoothingType.All);
        }

        protected override bool CheckForGround() {
            var ray = new Ray() {
                origin = Body.position,
                direction = -transform.up
            };

            if (Physics.Raycast(ray, out var hit, Collider.height * .5f + m_groundCastDistance, m_groundMask)) {
                if (hit.normal.y < Mathf.Sin(m_groundSlopeLimit * Mathf.Deg2Rad)) {
                    GroundNormal = Vector3.up;
                    GroundTangent = m_orientation.forward;
                    GroundBody = null;
                    return false;
                }

                GroundNormal = hit.normal;
                GroundTangent = m_orientation.TransformDirection(Quaternion.Euler(90, 0, 0) * m_orientation.InverseTransformDirection(GroundNormal));
                GroundBody = hit.rigidbody;
                return true;
            }

            GroundNormal = Vector3.up;
            GroundTangent = Vector3.right;
            GroundBody = null;

            return false;
        }

        protected override bool CheckForWalls() {
            if (IsGrounded) {
                WallNormal = Vector3.zero;
                WallTangent = Vector3.zero;
                WallSide = 0;
                return false;
            }

            var ray = new Ray() {
                origin = Body.position,
                direction = m_orientation.right
            };

            var hitting = IsWalled ? Physics.SphereCast(ray, .25f, out var hit, m_collider.radius + m_wallsCastDistance, m_wallsMask) : Physics.Raycast(ray, out hit, m_collider.radius + m_wallsCastDistance, m_wallsMask);

            if (hitting) {
                WallNormal = hit.normal;
                WallTangent = Vector3.Cross(WallNormal, transform.up);
                WallSide = 1;
                return true;
            }

            ray.direction = -m_orientation.right;
            hitting = IsWalled ? Physics.SphereCast(ray, .25f, out hit, m_collider.radius + m_wallsCastDistance, m_wallsMask) : Physics.Raycast(ray, out hit, m_collider.radius + m_wallsCastDistance, m_wallsMask);

            if (hitting) {
                WallNormal = hit.normal;
                WallTangent = Vector3.Cross(WallNormal, transform.up);
                WallSide = -1;
                return true;
            }

            WallNormal = Vector3.zero;
            WallTangent = Vector3.zero;
            WallSide = 0;
            return false;
        }

        protected override void ApplyPhysics() {
            Collider.height = Height;

            if (_moveAlongPlane) {
                _moveDirection = Vector3.ProjectOnPlane(m_orientation.TransformDirection(Direction), _moveAlongPlaneNormal);
                _moveDirection -= _moveAlongPlaneNormal;
                _moveAlongPlane = false;
            }
            else
                _moveDirection = Vector3.ProjectOnPlane(m_orientation.TransformDirection(Direction), GroundNormal);

            if (IsGrounded)
                Body.AddForce(10f * MoveSpeed * m_responsiveness * _moveDirection, ForceMode.Acceleration);
            else
                Body.AddForce(10f * MoveSpeed * m_responsiveness * m_airborneResponsiveness * _moveDirection, ForceMode.Acceleration);

            SpeedPercent = Velocity.magnitude / MoveSpeed;

            if (IsGrounded && !IsAiring)
                Velocity = Vector3.ClampMagnitude(Velocity, MoveSpeed);
            else {
                var flatVel = Velocity;
                flatVel.y = 0f;
                if (flatVel.magnitude > MoveSpeed) {
                    flatVel = Vector3.ClampMagnitude(flatVel, MoveSpeed);
                    Velocity = new(flatVel.x, Velocity.y, flatVel.z);
                }
            }

#if UNITY_6000
            Body.linearDamping = Drag;
#else
            Body.drag = Drag;
#endif
        }
        #endregion

        #region Public Methods (Movement)
        public void Look(Vector2 delta) {
            _angleY += delta.x * m_sensitivityX;
            _angleX -= delta.y * m_sensitivityY;
            _angleX = Mathf.Clamp(_angleX, -90f, 90f);
        }

        public void Move(Vector2 direction) {
            Direction = Vector3.ClampMagnitude(new Vector3(direction.x, 0f, direction.y), 1f);

            if (Mathf.Abs(Direction.x) > .01f)
                _lastNonZeroInputX = (int)Mathf.Sign(Direction.x);
        }

        public void MoveAlong(Vector2 direction, Vector3 normal) {
            Move(direction);
            _moveAlongPlane = true;
            _moveAlongPlaneNormal = normal;
        }

        public void Freeze() => Velocity = Vector3.zero;
        public void Freeze(SnapAxis axis) {
            var vel = Velocity;

            if (axis.HasFlag(SnapAxis.X))
                vel.x = 0;
            if (axis.HasFlag(SnapAxis.Y))
                vel.y = 0;
            if (axis.HasFlag(SnapAxis.Z))
                vel.z = 0;

            Velocity = vel;
        }

        public void Launch(Vector3 velocity) => Body.AddForce(velocity, ForceMode.VelocityChange);

        public void Throw(Vector3 impulse) => Body.AddForce(impulse, ForceMode.Impulse);

        public void Push(Vector3 force) => Body.AddForce(force, ForceMode.Force);

        public void Accelerate(Vector3 acceleration) => Body.AddForce(acceleration, ForceMode.Acceleration);

        public override Vector3 Reorient(in Vector3 input) => m_orientation.TransformDirection(input);
        #endregion

        #region Public Methods
        public void SetCursor(bool visible) {
            Cursor.visible = visible;
            Cursor.lockState = visible ? CursorLockMode.None : CursorLockMode.Locked;
        }

        public void SetMaxMoveSpeed(float maxSpeed) => m_moveSpeed.Value = maxSpeed;
        public void SetMaxMoveSpeedSudden(float maxSpeed) => MoveSpeed = m_moveSpeed.Value = maxSpeed;
        public void SetMaxMoveSpeedInertial(float maxSpeed) {
            m_moveSpeed.Value = maxSpeed;
        }

        public void ResetMaxMoveSpeed() => m_moveSpeed.Reset();
        public void ResetMaxMoveSpeedSudden() => MoveSpeed = m_moveSpeed.Reset();
        public void ResetMaxMoveSpeedInertial() {
            m_moveSpeed.Reset();
        }

        public void SetDrag(float drag) => m_drag.Value = drag;
        public void ResetDrag() => m_drag.Reset();
        public void ClearDrag() => m_drag.Value = 0f;

        public void SetHeight(float height) => m_height.Value = height;
        public void ResetHeight() => m_height.Reset();

        public void SetResponsiveness(float amount) => m_responsiveness.Value = amount;
        public void ResetResponsiveness() => m_responsiveness.Reset();

        public void SetSmoothing(PlatformerSmoothingType smoothingType, float value) {
            if (smoothingType.HasFlag(PlatformerSmoothingType.MoveSpeed))
                m_moveSpeedLerpQuickness.Value = value;
            if (smoothingType.HasFlag(PlatformerSmoothingType.VelocityControl))
                _smoothingVelocityControl = value;
        }

        public void ResetSmoothing(PlatformerSmoothingType smoothingType) {
            if (smoothingType.HasFlag(PlatformerSmoothingType.MoveSpeed))
                m_moveSpeedLerpQuickness.Reset();
            if (smoothingType.HasFlag(PlatformerSmoothingType.VelocityControl))
                _smoothingVelocityControl = 1;
        }
        #endregion

        #region Private Methods
        public static void DrawWireCapsule(Vector3 _pos, Quaternion _rot, float _radius, float _height, Color _color = default(Color))
        {
#if UNITY_EDITOR
            if (_color != default(Color))
                Handles.color = _color;
            Matrix4x4 angleMatrix = Matrix4x4.TRS(_pos, _rot, Handles.matrix.lossyScale);
            using (new Handles.DrawingScope(angleMatrix))
            {
                var pointOffset = (_height - (_radius * 2)) / 2;
 
                //draw sideways
                Handles.DrawWireArc(Vector3.up * pointOffset, Vector3.left, Vector3.back, -180, _radius);
                Handles.DrawLine(new Vector3(0, pointOffset, -_radius), new Vector3(0, -pointOffset, -_radius));
                Handles.DrawLine(new Vector3(0, pointOffset, _radius), new Vector3(0, -pointOffset, _radius));
                Handles.DrawWireArc(Vector3.down * pointOffset, Vector3.left, Vector3.back, 180, _radius);
                //draw frontways
                Handles.DrawWireArc(Vector3.up * pointOffset, Vector3.back, Vector3.left, 180, _radius);
                Handles.DrawLine(new Vector3(-_radius, pointOffset, 0), new Vector3(-_radius, -pointOffset, 0));
                Handles.DrawLine(new Vector3(_radius, pointOffset, 0), new Vector3(_radius, -pointOffset, 0));
                Handles.DrawWireArc(Vector3.down * pointOffset, Vector3.back, Vector3.left, -180, _radius);
                //draw center
                Handles.DrawWireDisc(Vector3.up * pointOffset, Vector3.up, _radius);
                Handles.DrawWireDisc(Vector3.down * pointOffset, Vector3.up, _radius);
 
            }
#endif
        }
        #endregion
    }
}