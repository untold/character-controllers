using CharacterControllers.Runtime.Core;
using CharacterControllers.Runtime.Utility;
using UnityEditor;
using UnityEngine;

namespace CharacterControllers.Runtime.Components {
    public class IsometricKinematicController : KinematicControllerBase {
        #region Public Variables
        [Header("References")]
        [SerializeField] private CapsuleCollider m_collider;
        [SerializeField] private Animator m_animator;

        [Header("Settings")]
        [SerializeField] private float m_gravityWeight = 1f;
        [SerializeField] private float m_moveAcceleration = 2f;
        [SerializeField] private float m_maxMoveVelocity = 4f;
        [SerializeField] private float m_maxFallingVelocity = 12f;

        [Space]
        [SerializeField] private float m_drag = 1f;
        [SerializeField] private float m_groundDrag = 4f;

        [Header("Collisions")]
        [SerializeField] private float m_collisionThickness = 0.1f;
        [SerializeField] private float m_collisionCastDistance = 0.2f;
        [SerializeField] private LayerMask m_collisionMask = ~0;

        [Header("Ground Casting")]
        [SerializeField] private float m_groundCastDistance = .15f;
        [SerializeField] private LayerMask m_groundMask = 1;
        #endregion

        #region Private Variables
        private Vector3 _actualMotion;
        private Vector3 _desiredMotion;
        #endregion

        #region Properties
        public float SpeedPercent { get; private set; }
        public float GravityWeight => m_gravityWeight;

        public Vector3 Input { get; private set; }

        public CapsuleCollider Collider => m_collider;
        public Rigidbody GroundBody { get; private set; }
        #endregion

        #region Behaviour Callbacks
#if UNITY_EDITOR
        protected override void OnDrawGizmos() {
            base.OnDrawGizmos();

            var originalColor = Gizmos.color;
            var localUp = transform.up;
            var position = transform.TransformPoint(m_collider.center);

            // Ground casting ray
            Gizmos.color = IsGrounded ? Color.green : IsCoyoteTime ? Color.yellow : Color.red;
            var botPoint = transform.TransformPoint(m_collider.center + (m_collider.height * .5f - m_collider.radius) * Vector3.down);
            Gizmos.DrawRay(botPoint, Vector3.down * (m_collider.radius + m_groundCastDistance));

            // Ground casting sphere
            // Gizmos.color *= .8f;
            // Gizmos.DrawSphere(position - localUp * (m_collider.height * .5f) + localUp * m_groundCastOffsetY - localUp * m_groundCastDistance, m_groundCastRadius);

            Gizmos.color = originalColor;

            Gizmos.DrawRay(transform.position, Velocity);

            Handles.color = Color.yellow;
            Handles.DrawLine(transform.position, transform.position + _desiredMotion * 10f, 5f);
            Handles.color = Color.green;
            Handles.DrawLine(transform.position, transform.position + _actualMotion * 10f, 5f);
            Handles.color = Color.white;
        }
#endif
        #endregion

        #region Overrides
        protected override bool OnGroundCheck(out RaycastHit hit) {
            // if (!Caster.CapsuleCast(m_collider, Vector3.down, m_collisionCastDistance, out hit, m_collisionThickness, m_groundMask)) {
            var botPoint = transform.TransformPoint(m_collider.center + (m_collider.height * .5f - m_collider.radius) * Vector3.down);
            if (!Physics.Raycast(botPoint, Vector3.down, out hit, m_collider.radius + m_groundCastDistance, m_groundMask)) {
                GroundNormal = Vector3.up;
                GroundTangent = Vector3.right;
                GroundPoint = transform.TransformPoint((m_collider.height * .5f + m_collider.radius) * Vector3.down);
                if (GroundBody)
                    transform.SetParent(null, true);
                GroundBody = null;
                return false;
            }

            GroundNormal = hit.normal;
            GroundTangent = Quaternion.Euler(0, 0, -90) * GroundNormal;
            GroundPoint = hit.point;
            if (hit.rigidbody && GroundBody != hit.rigidbody)
                transform.SetParent(hit.transform, true);
            GroundBody = hit.rigidbody;
            return true;
        }

        protected override void OnApplyPhysics() {
            // Apply input
            if (Input.sqrMagnitude > .01f)
                Velocity += Time.fixedDeltaTime * m_moveAcceleration * Input;

            if (!IsGrounded && !Mathf.Approximately(m_gravityWeight, 0) && Velocity.y > -m_maxFallingVelocity)
                Velocity += Time.fixedDeltaTime * m_gravityWeight * Physics.gravity;

            if (IsGrounded)
                Velocity = Vector3.ProjectOnPlane(Velocity, GroundNormal);

            Velocity = ApplyDragSmart(Velocity, IsGrounded ? m_groundDrag : m_drag);

            /*if (GroundBody)
                Velocity += GroundBody.GetPointVelocity(GroundPoint);*/

            var deltaMotion = Velocity * Time.deltaTime;
            // CastAndCorrectPosition(Body.position, deltaMotion, ref deltaMotion, true);
            Body.MovePosition(Body.position + deltaMotion);
            Body.PublishTransform();

            /*// Apply gravity
            if (!IsGrounded && !Mathf.Approximately(m_gravityWeight, 0) && Velocity.y > -m_maxFallingVelocity)
                Velocity += Time.fixedDeltaTime * m_gravityWeight * Physics.gravity;

            if (IsGrounded) {
                var project = Vector3.Project(Velocity, GroundNormal);
                var projLocal = transform.InverseTransformDirection(project);
                if (projLocal.y > -.5f)
                    Velocity -= Time.fixedDeltaTime * m_gravityWeight * m_groundAdherenceForce * GroundNormal;
            }

            // Apply viscosity (drag)
            Velocity = ApplyDragSmart(Velocity, m_drag);

            // Ground adjustments
            if (IsGrounded) {
                Velocity = Vector3.ProjectOnPlane(Velocity, GroundNormal);

                if (Input.sqrMagnitude < .01f)
                    Velocity = ApplyDragSmart(Velocity, m_groundDrag);
            }

            var deltaMotion = Time.fixedDeltaTime * Velocity;
            _desiredMotion = deltaMotion;

            // Collision adjustments
            CastAndCorrectPosition(Body.position, deltaMotion, ref _desiredMotion);

            // Move
            Body.MovePosition(Body.position + deltaMotion);
            Body.PublishTransform();*/
        }
        #endregion

        #region Public Methods
        public void Move(Vector3 direction) => Input = direction;
        #endregion

        #region Private Methods
        private Vector3 ApplyDragSmart(Vector3 velocity, float drag) {
            var clampedDrag = Mathf.Max(0, drag);
            if (clampedDrag < 1)
                velocity *= 1 - clampedDrag * Time.fixedDeltaTime;
            else
                velocity -= clampedDrag * Time.fixedDeltaTime * velocity;

            return velocity;
        }

        private void CastAndCorrectPosition(Vector3 position, Vector3 moveDelta, ref Vector3 finalMoveDelta, bool firstIteration = false) {
            if (moveDelta.magnitude < .01f)
                return;

            var magnitude = moveDelta.magnitude;
            var ray = new Ray {
                origin = position,
                direction = moveDelta.normalized
            };

            // var didHit = Physics.SphereCast(ray, m_collider.radius, out var hit, magnitude, m_collisionMask, QueryTriggerInteraction.Ignore);
            var didHit = Caster.CapsuleCast(m_collider, moveDelta.normalized, magnitude, out var hit, m_collisionThickness, m_collisionMask);

            if (!didHit) {
                if (firstIteration)
                    finalMoveDelta = moveDelta;

                return;
            }

            var remainingMagnitude = magnitude - hit.distance;
            var tangent = Vector3.ProjectOnPlane(ray.direction, hit.normal).normalized;
            var actualDeltaMove = ray.direction * hit.distance + hit.normal * m_collisionThickness;
            finalMoveDelta += actualDeltaMove;
            CastAndCorrectPosition(position + actualDeltaMove, tangent * remainingMagnitude, ref finalMoveDelta);
        }
        #endregion

        #region Event Methods
        #endregion
    }
}