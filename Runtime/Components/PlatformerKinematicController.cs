﻿using CharacterControllers.Runtime.Core;
using CharacterControllers.Runtime.Utility;
using UnityEngine;

namespace CharacterControllers.Runtime.Components {
    [AddComponentMenu("Character Controllers/Platformer Controller (Kinematic)")]
    public class PlatformerKinematicController : KinematicControllerBase {
        #region Public Variables
        [Header("References")]
        [SerializeField] private CapsuleCollider m_collider;
        [SerializeField] private Animator m_animator;

        [Header("Settings")]
        [SerializeField] private float m_gravityWeight = 1f;
        [SerializeField] private float m_moveAcceleration = 2f;
        [SerializeField] private float m_maxMoveVelocity = 4f;
        [SerializeField] private float m_maxFallingVelocity = 12f;

        [Space]
        [SerializeField] private float m_collisionThickness = 0.1f;
        [SerializeField] private float m_collisionCastDistance = 0.2f;
        [SerializeField] private LayerMask m_collisionMask = ~0;

        [Space]
        [SerializeField] private float m_drag = 1f;
        [SerializeField] private float m_groundDrag = 4f;

        [Header("Ground Casting")]
        [SerializeField] private float m_groundCastDistance = .15f;
        [SerializeField] private float m_groundCastOffsetY = .05f;
        [SerializeField] private float m_groundCastRadius = .15f;
        [SerializeField] private float m_groundAdherenceForce = 5f;
        [SerializeField] private LayerMask m_groundMask = 1;
        #endregion

        #region Private Variables
        private Vector3 _actualMotion;
        private Vector3 _desiredMotion;
        #endregion

        #region Properties
        public float SpeedPercent { get; private set; }
        public float GravityWeight => m_gravityWeight;

        public Vector2 Input { get; private set; }

        public CapsuleCollider Collider => m_collider;
        #endregion

        #region Behaviour Callbacks
        protected override void OnValidate() {
            base.OnValidate();

            m_collider ??= GetComponent<CapsuleCollider>();
            m_animator ??= GetComponentInChildren<Animator>();
        }

        private void Update() {
            SpeedPercent = IsGrounded
                ? Velocity.magnitude / m_maxMoveVelocity
                : Velocity.x / m_maxMoveVelocity;
        }

        protected override void OnDrawGizmos() {
            base.OnDrawGizmos();

            var originalColor = Gizmos.color;
            var localUp = transform.up;
            var position = transform.TransformPoint(m_collider.center);

            // Ground casting ray
            Gizmos.color = IsGrounded ? Color.green : IsCoyoteTime ? Color.yellow : Color.red;
            Gizmos.DrawRay(position - localUp * (m_collider.height * .5f) + localUp * m_groundCastOffsetY, -localUp * m_groundCastDistance);

            // Ground casting sphere
            Gizmos.color *= .8f;
            Gizmos.DrawSphere(position - localUp * (m_collider.height * .5f) + localUp * m_groundCastOffsetY - localUp * m_groundCastDistance, m_groundCastRadius);

            Gizmos.color = originalColor;

            Gizmos.DrawRay(transform.position, Velocity);

            UnityEditor.Handles.color = Color.yellow;
            UnityEditor.Handles.DrawLine(transform.position, transform.position + _desiredMotion * 10f, 5f);
            UnityEditor.Handles.color = Color.green;
            UnityEditor.Handles.DrawLine(transform.position, transform.position + _actualMotion * 10f, 5f);
            UnityEditor.Handles.color = Color.white;

            /*var topPoint = transform.TransformPoint(Collider.center + Vector3.up * (Collider.height * .5f - Collider.radius));
            var botPoint = transform.TransformPoint(Collider.center + Vector3.down * (Collider.height * .5f - Collider.radius));
            var radius = Collider.radius + m_collisionThickness;

            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(topPoint + _desiredMotion, radius);
            Gizmos.DrawWireSphere(botPoint + _desiredMotion, radius);

            Gizmos.color = originalColor;
            Gizmos.DrawWireSphere(topPoint + _actualMotion, radius);
            Gizmos.DrawWireSphere(botPoint + _actualMotion, radius);*/
        }
        #endregion

        #region Overrides
        /*protected override bool OnGroundCheck(out RaycastHit hit) {
            var position = transform.TransformPoint(m_collider.center);

            var ray = new Ray {
                origin = position - transform.up * (m_collider.height * .5f) + transform.up * m_groundCastOffsetY,
                direction = -transform.up
            };

            if (Physics.SphereCast(ray, m_groundCastRadius, out hit, m_groundCastDistance, m_groundMask)) {
                GroundNormal = new Vector3(hit.normal.x, hit.normal.y, 0);
                GroundTangent = Quaternion.Euler(0, 0, -90) * GroundNormal;
                GroundPoint = hit.point;
                return true;
            }

            GroundNormal = Vector3.up;
            GroundTangent = Vector3.right;
            GroundPoint = Body.position;
            return false;
        }*/

        protected override bool OnGroundCheck(out RaycastHit hit) {
            if (!Caster.CapsuleCast(m_collider, Vector3.down, m_groundCastDistance, out hit, m_collisionThickness, m_groundMask)) {
                GroundNormal = Vector3.up;
                GroundTangent = Vector3.right;
                GroundPoint = transform.TransformPoint((m_collider.height * .5f + m_collider.radius) * Vector3.down);
                return false;
            }

            GroundNormal = hit.normal;
            GroundTangent = Quaternion.Euler(0, 0, -90) * GroundNormal;
            GroundPoint = hit.point;
            return true;
        }

        protected override void OnApplyPhysics() {
            // Apply input
            if (Input.sqrMagnitude > .01f)
                Velocity += Time.fixedDeltaTime * m_moveAcceleration * Input.x * GroundTangent;

            // Apply gravity
            if (!IsGrounded && !Mathf.Approximately(m_gravityWeight, 0) && Velocity.y > -m_maxFallingVelocity)
                Velocity += Time.fixedDeltaTime * m_gravityWeight * Physics.gravity;

            if (IsGrounded) {
                var project = Vector3.Project(Velocity, GroundNormal);
                var projLocal = transform.InverseTransformDirection(project);
                if (projLocal.y > -.5f)
                    Velocity -= Time.fixedDeltaTime * m_gravityWeight * m_groundAdherenceForce * GroundNormal;
            }

            // Apply viscosity (drag)
            Velocity = ApplyDragSmart(Velocity, m_drag);

            // Ground adjustments
            if (IsGrounded) {
                Velocity = Vector3.ProjectOnPlane(Velocity, GroundNormal);

                if (Input.sqrMagnitude < .01f)
                    Velocity = ApplyDragSmart(Velocity, m_groundDrag);
            }

            var deltaMotion = Time.fixedDeltaTime * Velocity;
            _desiredMotion = deltaMotion;

            // Collision adjustments


            // Move
            Body.MovePosition(Body.position + deltaMotion);
            Body.PublishTransform();
        }
        #endregion

        #region Public Methods
        public void Move(Vector2 direction) => Input = direction;
        #endregion

        #region Private Methods
        private Vector3 ApplyDragSmart(Vector3 velocity, float drag) {
            var clampedDrag = Mathf.Max(0, drag);
            if (clampedDrag < 1)
                velocity *= 1 - clampedDrag * Time.fixedDeltaTime;
            else
                velocity -= clampedDrag * Time.fixedDeltaTime * velocity;

            return velocity;
        }

        private bool CastHit(ref Vector3 motion) {
            if (!Caster.CapsuleCast(m_collider, motion.normalized, motion.magnitude, out var hit, m_collisionThickness, m_collisionMask))
                return false;

            motion = Vector3.ClampMagnitude(motion, hit.distance);
            motion = Vector3.ProjectOnPlane(motion, hit.normal);
            return true;
        }
        #endregion
    }
}