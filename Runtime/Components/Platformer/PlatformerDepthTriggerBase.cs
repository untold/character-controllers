﻿using UnityEngine;

namespace CharacterControllers.Runtime.Components.Platformer {
    public enum DepthMode {
        Set,
        Add,
        Subtract
    }

    public abstract class PlatformerDepthTriggerBase : MonoBehaviour, System.IComparable<PlatformerDepthTriggerBase> {
        #region Public Variables
        [field: SerializeField] public int Priority { get; private set; }
        [field: SerializeField] public DepthMode Mode { get; private set; }
        #endregion

        #region Behaviour Callbacks
        protected virtual void Reset() {
            gameObject.layer = 2; // Ignore Raycast
        }

        protected virtual void OnValidate() {
            if (!TryGetComponent(out Collider trigger)) {
                gameObject.AddComponent<BoxCollider>().isTrigger = true;
                return;
            }

            trigger.isTrigger = true;
        }

        private void OnTriggerEnter(Collider other) {
            if (!other.TryGetComponent(out IDepthController controller))
                return;

            controller.AddDepthTrigger(this);
        }

        private void OnTriggerExit(Collider other) {
            if (!other.TryGetComponent(out IDepthController controller))
                return;

            controller.RemoveDepthTrigger(this);
        }
        #endregion

        #region Public Methods
        public abstract float GetDepthAt(Vector3 position);
        #endregion

        #region Interface (IComparable<>)
        public int CompareTo(PlatformerDepthTriggerBase other) => Priority - other.Priority;
        #endregion
    }
}