namespace CharacterControllers.Runtime.Components.Platformer {
    public interface IDepthController {
        #region Public Methods
        public void AddDepthTrigger(PlatformerDepthTriggerBase trigger);

        public void RemoveDepthTrigger(PlatformerDepthTriggerBase trigger);
        #endregion
    }
}