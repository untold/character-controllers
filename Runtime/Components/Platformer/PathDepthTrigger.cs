﻿#if SPLINES && MATH
using UnityEngine;
using UnityEngine.Splines;

namespace CharacterControllers.Runtime.Components.Platformer {
    [RequireComponent(typeof(SplineContainer))]
    public sealed class PathDepthTrigger : PlatformerDepthTriggerBase {
        #region Public Variables
        [SerializeField] private SplineContainer m_spline;
        #endregion

        #region Behaviour Callbacks
        protected override void OnValidate() {
            m_spline ??= GetComponent<SplineContainer>();

            base.OnValidate();
        }

        private void Awake() {
            m_spline ??= GetComponent<SplineContainer>();
        }

        private void OnDrawGizmos() {
            if (!m_spline || m_spline.Spline == null)
                return;

            var p0 = (Vector3)m_spline.Spline[0].Position;
            var p1 = (Vector3)m_spline.Spline[^1].Position;
            Gizmos.DrawSphere(p0 + transform.position, .25f);
            Gizmos.DrawSphere(p1 + transform.position, .25f);
        }
        #endregion

        #region Overrides
        public override float GetDepthAt(Vector3 position) {
            var p0 = (Vector3)m_spline.Spline[0].Position + transform.position;
            var p1 = (Vector3)m_spline.Spline[^1].Position + transform.position;
            var t = InverseLerp(p0, p1, position);

            var pos = (Vector3)m_spline.EvaluatePosition(t);
            return pos.z;
        }
        #endregion

        #region Private Methods
        private static float InverseLerp(Vector3 a, Vector3 b, Vector3 value) {
            var ab = b - a;
            var av = value - a;
            return Vector3.Dot(av, ab) / Vector3.Dot(ab, ab);
        }
        #endregion
    }
}
#endif