using UnityEngine;

namespace CharacterControllers.Runtime.Components.Platformer {
    public sealed class SimpleDepthTrigger : PlatformerDepthTriggerBase {
        #region Public Variables
        [SerializeField] private float m_fixedDepth = 0.33f;
        #endregion

        #region Overrides
        public override float GetDepthAt(Vector3 position) => m_fixedDepth;
        #endregion
    }
}