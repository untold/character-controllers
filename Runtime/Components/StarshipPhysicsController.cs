using CharacterControllers.Runtime.Utility;
using UnityEngine;

namespace CharacterControllers.Runtime.Components {
    [AddComponentMenu("Character Controllers/Starship Controller (Physics)")]
    public class StarshipPhysicsController : MonoBehaviour {
        #region Public Variables
        [Header("Debug")]
        [SerializeField] private bool drawVectors;

        [Header("Components")]
        [SerializeField] private Rigidbody m_rigidbody;

        [Header("Settings")]
        [SerializeField] private DefaultedValue<float> m_pushForce;
        [SerializeField] private DefaultedValue<float> m_slideForce;

        [Space]
        [SerializeField] private DefaultedValue<float> m_rollForce;
        [SerializeField] private DefaultedValue<float> m_yawForce;
        [SerializeField] private DefaultedValue<float> m_pitchForce;

        [Space]
        [SerializeField, Min(1f)] private float m_lookAtQuickness = 8f;
        [SerializeField, Min(0f)] private float m_lookAtTimeRange = 0.6f;
        [SerializeField, Range(0, 90f)] private float m_lookAtMaxAngle = 35f;
        [SerializeField] private AnimationCurve m_lookAtProgression = AnimationCurve.EaseInOut(0, 0, 1, 1);
        #endregion

        #region Private Variables
        private float _lookAtTime;
        private float _lookAtWeight;
        private Vector3 _lookAtPoint;
        #endregion

        #region Properties
        public Vector3 Movement { get; private set; }
        public Vector3 Rotation { get; private set; }

        public Rigidbody Body => m_rigidbody;
        #endregion

        #region Behaviour Callbacks
        protected virtual void Reset() {
            m_rigidbody ??= GetComponent<Rigidbody>();
            m_rigidbody.useGravity = false;
        }

        protected virtual void OnValidate() {
            m_rigidbody ??= GetComponent<Rigidbody>();
            m_rigidbody.useGravity = false;
        }

        private void FixedUpdate() {
            ApplyMotionForces();
            ApplyRotationalForces();

            if (_lookAtWeight > .01f)
                HandleFacing();
        }
        #endregion

        #region Overrides
        #endregion

        #region Public Methods
        public void Move(Vector3 input) => Movement = input;
        public void ResetMove() => Movement = Vector3.zero;

        public void Rotate(Vector3 angles) => Rotation = angles;
        public void RotateSmooth(Vector3 angles, float quickness = 4f) => Rotation = Vector3.Lerp(Rotation, angles, Time.deltaTime * 4f);
        public void ResetRotate() => Rotation = Vector3.zero;

        public void PointAt(Vector3 targetPoint, float weight) {
            _lookAtPoint = targetPoint;
            _lookAtWeight = weight;

            if (_lookAtWeight <= .01f)
                _lookAtTime = 0f;
        }
        #endregion

        #region Private Methods
        private void ApplyMotionForces() {
            var pushForce = Movement.z * m_pushForce;
            var slideXForce = Movement.x * m_slideForce;
            var slideYForce = Movement.y * m_slideForce;

            Body.AddRelativeForce(slideXForce, slideYForce, pushForce, ForceMode.Acceleration);
        }

        private void ApplyRotationalForces() {
            var pitchForce = Rotation.x * m_pitchForce;
            var yawForce = Rotation.y * m_yawForce;
            var rollForce = Rotation.z * m_rollForce;

            Body.AddRelativeTorque(pitchForce, yawForce, rollForce, ForceMode.Acceleration);
        }

        private void HandleFacing() {
            _lookAtTime = Mathf.Min(_lookAtTime + Time.fixedDeltaTime, m_lookAtTimeRange);
            var timeWeight = _lookAtTime / m_lookAtTimeRange;

            var targetDir = _lookAtPoint - transform.position;
            targetDir.Normalize();
            var weight = m_lookAtProgression.Evaluate(_lookAtWeight * timeWeight);
            var factor = Time.fixedDeltaTime * m_lookAtQuickness * weight;

            var angle = Vector3.Angle(targetDir, transform.forward);
            var angleWeighted = .9f * (1f - Mathf.Clamp01(angle / m_lookAtMaxAngle)) + .1f;
            factor *= angleWeighted * timeWeight;

            var lookDir = Quaternion.LookRotation(targetDir);
            var interpolatedRot = Quaternion.Slerp(Body.rotation, lookDir, factor);

            Body.MoveRotation(interpolatedRot);

            Rotation = Vector3.Lerp(Rotation, Vector3.zero, factor);
        }
        #endregion

        #region Event Methods
        #endregion
    }
}