using System.Collections.Generic;
using CharacterControllers.Runtime.Components.Platformer;
using CharacterControllers.Runtime.Core;
using CharacterControllers.Runtime.Utility;
using UnityEngine;

namespace CharacterControllers.Runtime.Components {
    [System.Flags] public enum PlatformerSmoothingType { None = 0, MoveSpeed = 1, VelocityControl = 2, All = ~0 }

    [AddComponentMenu("Character Controllers/Platformer Controller (Physics)")]
    [RequireComponent(typeof(CapsuleCollider))]
    public class PlatformerPhysicsController : PhysicsControllerBase, IDepthController {
        #region Public Variables
        [Header("Components")]
        [SerializeField] private CapsuleCollider m_collider;
        [SerializeField] private Animator m_animator;

        [Header("Settings")]
        [SerializeField] private DefaultedValue<float> m_moveSpeed = 6f;
        [SerializeField] private DefaultedValue<float> m_adhesionForce = 1f;
        [SerializeField] private float m_acceleration = 8;
        [SerializeField] private float m_accelerationAirborne = 4;
        [SerializeField] private float m_deceleration = 2;
        [SerializeField] private float m_frictionGrounded = 1;
        [SerializeField] private float m_frictionAirborne = .5f;
        [SerializeField] private float m_velocityPower = 1;
        [Space(2)]
        [SerializeField] private DefaultedValue<float> m_moveSpeedLerpQuickness = 12;
        [SerializeField] private DefaultedValue<float> m_depth = 0;
        [SerializeField, Min(0.1f)] private float m_depthLerpQuickness = 4f;
        [SerializeField] private bool m_depthUsesSpeedPercent = true;

        [Header("Ground Casting")]
        [SerializeField] private float m_groundCastDistance = .15f;
        [SerializeField] private float m_groundCastOffsetY = .05f;
        [SerializeField] private float m_groundCastRadius = .15f;
        [SerializeField] private float m_groundSlopeLimit = 45f;
        [SerializeField] private float m_skinThickness = 0.05f;
        [SerializeField] private LayerMask m_groundMask = 1;

        [Header("Walls Casting")]
        [SerializeField] private float m_wallsCastDistance = .15f;
        [SerializeField] private LayerMask m_wallsMask = 1;
        #endregion

        #region Private Variables
        private float _smoothingVelocityControl = 1f;
        private int _lastNonZeroInputX = 1;
        private Ray _groundRay;
        private RaycastHit _groundHit;
        private bool _groundKeep;

        private readonly List<PlatformerDepthTriggerBase> _depthTriggers = new();
        #endregion

        #region Properties
        public CapsuleCollider Collider => m_collider;
        public Animator Animator => m_animator;

        protected float BaseDepth => m_depth;
        protected float MoveSpeed { get; set; }
        protected float AdhesionForce => m_adhesionForce;

        protected float SmoothingMoveSpeed => m_moveSpeedLerpQuickness;
        protected float SmoothingVelocityControl => _smoothingVelocityControl;

        public Vector2 Direction { get; private set; }

        public float SpeedPercent { get; private set; }
        public float LastValidInput => _lastNonZeroInputX;

        [field: SerializeField] public bool UseAdhesionForce { get; set; } = true;
        [field: SerializeField] public bool UseFriction { get; set; } = true;

        public Vector3 WallNormal { get; private set; }
        public LayerMask WallLayer { get; private set; }
        public Rigidbody GroundBody { get; private set; }
        #endregion

        #region Behaviour Callbacks
        protected override void OnValidate() {
            base.OnValidate();

            m_collider ??= GetComponent<CapsuleCollider>();
            m_animator ??= GetComponentInChildren<Animator>();
        }

        protected override void Update() {
            base.Update();

            MoveSpeed = Mathf.Lerp(MoveSpeed, m_moveSpeed, Time.deltaTime * m_moveSpeedLerpQuickness);
        }

        private void SetDepth() {
            var vel = Velocity;
            vel.z = 0;
            Velocity = vel;

            var pos = Body.position;
            pos.z = BaseDepth;

            var t = Time.deltaTime * m_depthLerpQuickness * (m_depthUsesSpeedPercent ? Mathf.Max(SpeedPercent, .5f) : 1f);

            foreach (var trigger in _depthTriggers) {
                switch (trigger.Mode) {
                    case DepthMode.Set:
                        pos.z = trigger.GetDepthAt(pos);
                        Body.MovePosition(Vector3.Lerp(Body.position, pos, t));
                        return;
                    case DepthMode.Add:
                        pos.z += trigger.GetDepthAt(pos);
                        break;
                    case DepthMode.Subtract:
                        pos.z -= trigger.GetDepthAt(pos);
                        break;
                    default:
                        continue;
                }
            }

            Body.MovePosition(Vector3.Lerp(Body.position, pos, t));
        }

        protected override void OnDrawGizmos() {
            base.OnDrawGizmos();

            var originalMatrix = Gizmos.matrix;
            var originalColor = Gizmos.color;
            var localUp = transform.up;
            var position = transform.TransformPoint(Collider.center);

            // Ground casting ray
            Gizmos.color = IsGrounded ? Color.green : IsCoyoteTime ? Color.yellow : Color.red;
            Gizmos.DrawRay(position - localUp * (m_collider.height * .5f) + localUp * m_groundCastOffsetY, -localUp * m_groundCastDistance);

            // Ground casting sphere
            Gizmos.color *= .8f;
            Gizmos.DrawSphere(position - localUp * (m_collider.height * .5f) + localUp * m_groundCastOffsetY - localUp * m_groundCastDistance, m_groundCastRadius);

            // Wall casting rays
            Gizmos.color = IsWalled ? Color.green : Color.red;
            Gizmos.DrawRay(position, Vector3.right * _lastNonZeroInputX * (m_collider.radius + m_wallsCastDistance));
            if (Mathf.Abs(Direction.x) < .01f)
                Gizmos.DrawRay(position, Vector3.left * _lastNonZeroInputX * (m_collider.radius + m_wallsCastDistance));

            /*Gizmos.matrix = transform.localToWorldMatrix;
            Gizmos.DrawWireCube(Vector3.zero, new(m_collider.radius * 2f, m_collider.height, m_collider.radius * 2f));
            Gizmos.matrix = originalMatrix;*/
            var castDist = m_skinThickness * 2f;
            var t = m_collider.transform;
            var topPoint = t.TransformPoint(m_collider.center + (m_collider.height * .5f - m_collider.radius) * Vector3.up);
            var botPoint = t.TransformPoint(m_collider.center + (m_collider.height * .5f - m_collider.radius) * Vector3.down);
            var radius = m_collider.radius - m_skinThickness;
            Gizmos.color = Color.gray;
            Gizmos.DrawWireSphere(topPoint, radius);
            Gizmos.DrawWireSphere(botPoint, radius);
            Gizmos.color = _groundKeep ? Color.green : Color.red;
            Gizmos.DrawWireSphere(topPoint - t.up * castDist, radius);
            Gizmos.DrawWireSphere(botPoint - t.up * castDist, radius);

            Gizmos.color = Color.black;

            if (GroundBody)
                Gizmos.DrawLine(Body.position, GroundBody.position);

            Gizmos.color = originalColor;
        }
        #endregion

        #region Overrides
        protected override void OnSetUp() {
            ResetMaxMoveSpeedSudden();
            ResetAdhesionForce();
            ResetBaseDepth();

            ResetSmoothing(PlatformerSmoothingType.All);
        }

        protected override bool CheckForGround() {
            var position = transform.TransformPoint(Collider.center);

            _groundRay = new Ray {
                origin = position - transform.up * (m_collider.height * .5f) + transform.up * m_groundCastOffsetY,
                direction = -transform.up
            };

            if (Physics.SphereCast(_groundRay, m_groundCastRadius, out _groundHit, m_groundCastDistance, m_groundMask)) {
                if (_groundHit.normal.y < Mathf.Sin(m_groundSlopeLimit * Mathf.Deg2Rad)) {
                    GroundNormal = Vector3.up;
                    GroundTangent = Vector3.right;
                    GroundBody = null;
                    return false;
                }

                GroundNormal = new Vector3(_groundHit.normal.x, _groundHit.normal.y, 0);
                GroundTangent = Quaternion.Euler(0, 0, -90/* * Direction.x.Sign()*/) * GroundNormal;
                GroundBody = _groundHit.rigidbody;
                return true;
            }

            GroundNormal = Vector3.up;
            GroundTangent = Vector3.right;
            GroundBody = null;

            return false;
        }

        protected override bool CheckForWalls() {
            WallLayer = 1 << 0;
            var position = transform.TransformPoint(Collider.center);

            var ray = new Ray {
                origin = position,
                direction = Vector3.right * _lastNonZeroInputX
            };

            if (Physics.Raycast(ray, out var hit, m_collider.radius + m_wallsCastDistance, m_wallsMask)) {
                WallNormal = new Vector3(hit.normal.x, hit.normal.y, 0);
                WallLayer = 1 << hit.collider.gameObject.layer;
                return true;
            }

            if (!(Mathf.Abs(Direction.x) < .01f))
                return false;

            ray.direction *= -1;
            if (!Physics.Raycast(ray, out hit, m_collider.radius + m_wallsCastDistance, m_wallsMask))
                return false;

            WallNormal = new Vector3(hit.normal.x, hit.normal.y, 0);
            WallLayer = 1 << hit.collider.gameObject.layer;
            return true;
        }

        protected override void ApplyPhysics() {
            var targetSpeed = Direction.x * MoveSpeed + Inertia.x;
            var speedDif = targetSpeed - Velocity.x;
            var accelRate = Mathf.Abs(targetSpeed) > .01f ? (IsGrounded ? m_acceleration : m_accelerationAirborne) : m_deceleration;
            var movement = Mathf.Pow(Mathf.Abs(speedDif) * accelRate, m_velocityPower) * Mathf.Sign(speedDif);
            Body.AddForce(GroundTangent * movement);

            if (Inertia.sqrMagnitude > .01f)
                Body.AddForce(Mathf.Pow(Inertia.magnitude, 2) * GroundTangent, ForceMode.Acceleration);

            if (UseFriction && Mathf.Abs(Direction.x) < .01f) {
                var localVar = Vector3.ProjectOnPlane(Velocity, GroundNormal);
                var amount = Mathf.Min(Mathf.Abs(localVar.x), IsGrounded ? m_frictionGrounded : m_frictionAirborne);
                amount *= Mathf.Sign(localVar.x);
                Body.AddForce(GroundTangent * -amount, ForceMode.Impulse);
            }

            if (GroundBody && UseAdhesionForce)
                AnchorToGround();

            if (IsGrounded && UseAdhesionForce)
                KeepOnGround();

            SetDepth();

            if (IsGrounded && !GroundBody) {
                if (Mathf.Abs(MoveSpeed) > .01f)
                    SpeedPercent = Velocity.magnitude / MoveSpeed;
            } else {
                if (Mathf.Abs(MoveSpeed) > .01f)
                    SpeedPercent = Mathf.Abs(Velocity.x) / MoveSpeed;
            }
        }
        #endregion

        #region Public Methods (Movement)
        public void Move(Vector2 direction) {
            Direction = direction;

            if (Mathf.Abs(Direction.x) > .01f)
                _lastNonZeroInputX = (int)Mathf.Sign(Direction.x);
        }
        public void ResetMove() => Direction = Vector2.zero;

        public void Freeze() => Velocity = Vector3.zero;
        public void Freeze(SnapAxis axis) {
            var vel = Velocity;

            if (axis.HasFlag(SnapAxis.X))
                vel.x = 0;
            if (axis.HasFlag(SnapAxis.Y))
                vel.y = 0;
            if (axis.HasFlag(SnapAxis.Z))
                vel.z = 0;

            Velocity = vel;
        }

        public void SetPosition(Vector2 point) => Body.MovePosition(new(point.x, point.y, BaseDepth));
        public void ChangeVelocity(Vector3 velocity) => Velocity = velocity;
        public void LerpVelocity(Vector3 velocity, float time) => Velocity = Vector3.Lerp(velocity, velocity, time);

        public void LaunchSimple(float magnitude) => Launch(m_animator.transform.forward * magnitude);
        public void Launch(Vector3 velocity) => Body.AddForce(velocity, ForceMode.VelocityChange);
        public void ThrowSimple(float magnitude) => Throw(m_animator.transform.forward * magnitude);
        public void Throw(Vector3 impulse) => Body.AddForce(impulse, ForceMode.Impulse);
        public void Push(Vector3 force) {
            if (SpeedPercent >= 1)
                force.x = 0;

            Body.AddForce(force, ForceMode.Force);
        }
        public void Accelerate(Vector3 acceleration) => Body.AddForce(acceleration, ForceMode.Acceleration);

        public void AddDepthTrigger(PlatformerDepthTriggerBase trigger) {
            _depthTriggers.Add(trigger);
            _depthTriggers.Sort();
        }

        public void RemoveDepthTrigger(PlatformerDepthTriggerBase trigger) => _depthTriggers.Remove(trigger);
        #endregion

        #region Public Methods
        public void SetBaseDepth(float depth) => m_depth.Value = depth;
        public void ResetBaseDepth() => m_depth.Reset();

        public void SetMaxMoveSpeed(float maxSpeed) => m_moveSpeed.Value = maxSpeed;
        public void SetMaxMoveSpeedSudden(float maxSpeed) => MoveSpeed = m_moveSpeed.Value = maxSpeed;
        public void ResetMaxMoveSpeed() => m_moveSpeed.Reset();
        public void ResetMaxMoveSpeedSudden() => MoveSpeed = m_moveSpeed.Reset();

        public void SetAdhesionForce(float adhesionForce) => m_adhesionForce.Value = adhesionForce;
        public void ResetAdhesionForce() => m_adhesionForce.Reset();

        public void SetSmoothing(PlatformerSmoothingType smoothingType, float value) {
            if (smoothingType.HasFlag(PlatformerSmoothingType.MoveSpeed))
                m_moveSpeedLerpQuickness.Value = value;
            if (smoothingType.HasFlag(PlatformerSmoothingType.VelocityControl))
                _smoothingVelocityControl = value;
        }

        public void ResetSmoothing(PlatformerSmoothingType smoothingType) {
            if (smoothingType.HasFlag(PlatformerSmoothingType.MoveSpeed))
                m_moveSpeedLerpQuickness.Reset();
            if (smoothingType.HasFlag(PlatformerSmoothingType.VelocityControl))
                _smoothingVelocityControl = 1;
        }
        #endregion

        #region Private Methods
        private void KeepOnGround() {
            _groundKeep = Body.SweepTest(-GroundNormal, out var hit, m_groundCastDistance * 2f, QueryTriggerInteraction.Ignore);
            if (_groundKeep)
                Body.MovePosition(Body.position - GroundNormal * hit.distance);
            // Body.AddForce(-GroundNormal * m_adhesionForce, ForceMode.Impulse);

            /*if (Body.linearVelocity.y > 0) {
                var vel = Body.linearVelocity;
                vel.y = 0;
                Body.linearVelocity = vel;
            }*/
        }

        private void AnchorToGround() {
            var pointVelocity = GroundBody.GetRelativePointVelocity(Body.position);
            Debug.DrawRay(transform.position, pointVelocity, Color.red, .02f);
            Body.MovePosition(Body.position + pointVelocity * Time.fixedDeltaTime);

            var dir = new Vector3 {
                x = Mathf.Sign(pointVelocity.x),
                y = -1
            }.normalized;
            Debug.DrawRay(transform.position, dir, Color.yellow, .02f);
            if (Body.SweepTest(-GroundNormal, out var hit, pointVelocity.magnitude * Time.fixedDeltaTime, QueryTriggerInteraction.Ignore))
                Body.MovePosition(Body.position - GroundNormal * (hit.distance - m_skinThickness));
            /*else
                Body.MovePosition(Body.position + pointVelocity * Time.fixedDeltaTime);*/

            /*if (Body.linearVelocity.y < 0)
                return;

            var vel = Body.linearVelocity;
            vel.y = 0;
            Body.linearVelocity = vel;*/
        }
        #endregion
    }
}