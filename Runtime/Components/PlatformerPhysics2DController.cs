using CharacterControllers.Runtime.Core;
using CharacterControllers.Runtime.Utility;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace CharacterControllers.Runtime.Components {
    [AddComponentMenu("Character Controllers/Physics/Platformer Controller (2D)")]
    [RequireComponent(typeof(CapsuleCollider2D))]
    public class PlatformerPhysics2DController : Physics2DControllerBase {
        #region Public Variables
        [Header("Controller Settings")]
        [SerializeField] private CapsuleCollider2D m_collider;
        [SerializeField] private SpriteRenderer m_orientationReference;
        [SerializeField, Min(0f)] private float m_colliderSkinThickness = .02f;
        [SerializeField, Min(0f)] private float m_groundCastDistance = .25f;
        [SerializeField, Min(0f)] private float m_wallCastDistance = .25f;
        [SerializeField] private LayerMask m_worldCollidersMask;

        [Space]
        [SerializeField] private DefaultedValue<float> m_maxMoveSpeed = 6f;
        [SerializeField] private float m_acceleration = 8f;
        [SerializeField] private float m_accelerationAirborne = 3f;
        [SerializeField] private float m_deceleration = 6f;
        [SerializeField] private DefaultedValue<float> m_velocityPower = 1f;

        [Space]
        [SerializeField] private DefaultedValue<float> m_frictionGrounded = 2f;
        [SerializeField] private DefaultedValue<float> m_frictionAirborne = 4f;
        [field: SerializeField] public bool UseFriction { get; set; } = true;
        [field: SerializeField] public bool UseAdhesionForce { get; set; } = true;
        [field: SerializeField] public bool MotionUsesGroundTangent { get; set; } = true;

        [Space]
        [SerializeField] private DefaultedValue<float> m_moveSpeedLerpQuickness = 2f;
        #endregion

        #region Private Variables
        private RaycastHit2D _hitInfo;
        #endregion

        #region Properties
        public Vector2 InputDirection { get; private set; }

        public float MoveSpeed { get; private set; }

        public CapsuleCollider2D Collider => m_collider;
        #endregion

        #region Behaviour Callbacks
        protected override void OnValidate() {
            base.OnValidate();

            m_collider = GetComponent<CapsuleCollider2D>();
            m_orientationReference = GetComponentInChildren<SpriteRenderer>();
        }

        protected override void Update() {
            base.Update();

            MoveSpeed = Mathf.Lerp(MoveSpeed, m_maxMoveSpeed, Time.deltaTime * m_moveSpeedLerpQuickness);
        }

        protected override void OnDrawGizmos() {
            base.OnDrawGizmos();

            if (!enabled)
                return;

            if (!drawVectors)
                return;

#if UNITY_EDITOR
            #region Draw Ground Check Gizmos
            var groundedColor = IsGrounded ? Color.green : (IsCoyoteTime ? Color.yellow : Color.red);
            Handles.color = groundedColor;

            var rect = new Rect {
                xMin = m_collider.bounds.min.x + m_colliderSkinThickness,
                xMax = m_collider.bounds.max.x - m_colliderSkinThickness,
                yMin = m_collider.bounds.min.y + m_colliderSkinThickness,
                yMax = m_collider.bounds.max.y - m_colliderSkinThickness
            };
            // UnityEditor.Handles.DrawSolidRectangleWithOutline(rect, Color.clear, groundedColor);
            var isHor = m_collider.direction == CapsuleDirection2D.Horizontal;
            var up = isHor ? Vector2.left : Vector2.up;
            var down = isHor ? Vector2.right : Vector2.down;
            var left = isHor ? Vector2.down : Vector2.left;
            var right = isHor ? Vector2.up : Vector2.right;

            var radius = (isHor ? m_collider.size.y : m_collider.size.x) * .5f - m_colliderSkinThickness;
            var size = isHor ? m_collider.size.x * .5f : m_collider.size.y * .5f;
            var topPoint = transform.TransformPoint(m_collider.offset + up * (size - radius - m_colliderSkinThickness));
            var botPoint = transform.TransformPoint(m_collider.offset + down * (size - radius - m_colliderSkinThickness));
            Handles.DrawWireArc(topPoint, Vector3.back, left, 180f, radius);
            Handles.DrawWireArc(botPoint, Vector3.back, right, 180f, radius);
            if (isHor) {
                Handles.DrawLine(new Vector3(topPoint.x, topPoint.y - radius), new Vector3(botPoint.x, topPoint.y - radius));
                Handles.DrawLine(new Vector3(topPoint.x, topPoint.y + radius), new Vector3(botPoint.x, topPoint.y + radius));
            }
            else {
                Handles.DrawLine(new Vector3(topPoint.x - radius, topPoint.y), new Vector3(topPoint.x - radius, botPoint.y));
                Handles.DrawLine(new Vector3(topPoint.x + radius, topPoint.y), new Vector3(topPoint.x + radius, botPoint.y));
            }

            var point = new Vector2 {
                x = rect.center.x,
                y = rect.yMin
            };
            Handles.DrawLine(point, point + Vector2.down * m_groundCastDistance, 2f);
            #endregion

            #region Draw Wall Check Gizmos
            var walledColor = IsWalled ? Color.green : (IsCoyoteTimeWallGrab ? Color.yellow : Color.red);
            Handles.color = walledColor;

            var rightRayOrigin = transform.TransformPoint(m_collider.offset + Vector2.right * (m_collider.size.x * .5f - m_colliderSkinThickness));
            var leftRayOrigin = transform.TransformPoint(m_collider.offset + Vector2.left * (m_collider.size.x * .5f - m_colliderSkinThickness));
            Handles.DrawLine(rightRayOrigin, rightRayOrigin + Vector3.right * m_wallCastDistance, 2f);
            Handles.DrawLine(leftRayOrigin, leftRayOrigin + Vector3.left * m_wallCastDistance, 2f);

            Handles.color = Color.white;
            #endregion

            if (EditorApplication.isPlaying) {
                Gizmos.color = Color.yellow;
                var origin = m_collider.ClosestPoint(GroundPoint);
                Gizmos.DrawLine(origin, GroundPoint);
                Gizmos.color = Color.white;
            }
#endif
        }
        #endregion

        #region Overrides
        protected override GroundInfo OnGroundCheck() {
            var colliderCenter = transform.TransformPoint(m_collider.offset);
            var insetSize = m_collider.size;
            insetSize.x -= m_colliderSkinThickness * 2;
            insetSize.y -= m_colliderSkinThickness * 2;
            _hitInfo = Physics2D.CapsuleCast(colliderCenter, insetSize, m_collider.direction, transform.localEulerAngles.z, Vector2.down, m_groundCastDistance, m_worldCollidersMask);

            if (!_hitInfo)
                return new GroundInfo {
                    Normal = Vector2.up,
                    Tangent = Vector2.right,
                    Point = transform.position,
                    DidHit = false
                };

            return new GroundInfo {
                Normal = _hitInfo.normal,
                Tangent = -Vector2.Perpendicular(_hitInfo.normal),
                Point = _hitInfo.point,
                Distance = _hitInfo.distance - m_colliderSkinThickness,
                DidHit = true
            };
        }

        protected override GroundInfo OnWallCheck() {
            var ray = new Ray2D {
                origin = transform.TransformPoint(m_collider.offset + Vector2.right * (m_collider.size.x * .5f - m_colliderSkinThickness)),
                direction = Vector2.right
            };

            var hit = Physics2D.Raycast(ray.origin, ray.direction, m_wallCastDistance, m_worldCollidersMask);
            if (hit)
                return new GroundInfo {
                    Normal = hit.normal,
                    Tangent = -Vector2.Perpendicular(hit.normal),
                    Point = hit.point,
                    Distance = hit.distance,
                    DidHit = true
                };

            ray = new Ray2D {
                origin = transform.TransformPoint(m_collider.offset + Vector2.left * (m_collider.size.x * .5f - m_colliderSkinThickness)),
                direction = Vector2.left
            };
            hit = Physics2D.Raycast(ray.origin, ray.direction, m_wallCastDistance, m_worldCollidersMask);
            if (hit)
                return new GroundInfo {
                    Normal = hit.normal,
                    Tangent = -Vector2.Perpendicular(hit.normal),
                    Point = hit.point,
                    Distance = hit.distance,
                    DidHit = true
                };

            return new GroundInfo {
                Normal = Vector2.left,
                Tangent = Vector2.up,
                Point = transform.position,
                Distance = 0f,
                DidHit = false
            };
        }

        protected override void ApplyPhysics() {
            if (IsGrounded && UseAdhesionForce)
                KeepOnGround();

            var motionTangent = MotionUsesGroundTangent ? GroundTangent : Vector2.right;

            var targetSpeed = InputDirection.x * MoveSpeed + Inertia.x;
            var speedDif = targetSpeed - Velocity.x;
            var accelRate = Mathf.Abs(targetSpeed) > .01f ? (IsGrounded ? m_acceleration : m_accelerationAirborne) : (IsGrounded ? m_deceleration : 0f);
            var movement = Mathf.Pow(Mathf.Abs(speedDif) * accelRate, m_velocityPower) * Mathf.Sign(speedDif);
            Body.AddForce(motionTangent * movement);

            if (Inertia.sqrMagnitude > .01f)
                Body.AddForce(Mathf.Pow(Inertia.magnitude, 2) * motionTangent, ForceMode2D.Force);

            if (UseFriction && Mathf.Abs(InputDirection.x) < .01f) {
                var localVar = Vector3.ProjectOnPlane(Velocity, GroundNormal);
                var amount = Mathf.Min(Mathf.Abs(localVar.x), IsGrounded ? m_frictionGrounded : m_frictionAirborne);
                amount *= Mathf.Sign(localVar.x);
                Body.AddForce(motionTangent * -amount, ForceMode2D.Impulse);
            }

            /*if (GroundBody && UseAdhesionForce)
                AnchorToGround();*/
        }

        protected override void OnShutdown() {
            m_collider.enabled = false;
            Body.simulated = false;
        }
        #endregion

        #region Public Methods (Movement)
        public void Move(Vector2 direction) => InputDirection = direction;

        public void ResetMove() => InputDirection = Vector2.zero;

        public void Freeze() => Velocity = Vector2.zero;
        public void Freeze(SnapAxis axis) {
            var vel = Velocity;

            if (axis.HasFlag(SnapAxis.X))
                vel.x = 0;
            if (axis.HasFlag(SnapAxis.Y))
                vel.y = 0;

            Velocity = vel;
        }

        public void SetPosition(Vector2 point) => Body.MovePosition(point);
        public void ChangeVelocity(Vector2 velocity) => Velocity = velocity;
        public void LerpVelocity(Vector2 velocity, float time) => Velocity = Vector3.Lerp(velocity, velocity, time);

        public void LaunchSimple(float magnitude) => Launch(magnitude * (m_orientationReference.flipX ? -1 : 1) * Vector2.right);
        public void Launch(Vector2 velocity) => Body.AddForce(velocity, ForceMode2D.Impulse);
        public void ThrowSimple(float magnitude) => Throw(magnitude * (m_orientationReference.flipX ? -1 : 1) * Vector2.right);
        public void Throw(Vector2 impulse) => Body.AddForce(impulse, ForceMode2D.Impulse);
        public void Push(Vector2 force) => Body.AddForce(force, ForceMode2D.Force);
        public void Accelerate(Vector2 acceleration) => Body.AddForce(acceleration * Body.mass, ForceMode2D.Force);
        #endregion

        #region Public Methods
        public void SetMoveSpeed(float moveSpeed) => m_maxMoveSpeed.Value = moveSpeed;
        public void SetMoveSpeedSudden(float moveSpeed) => MoveSpeed = m_maxMoveSpeed.Value = moveSpeed;

        public void ResetMoveSpeed() => m_maxMoveSpeed.Reset();
        public void ResetMoveSpeedSudden() => MoveSpeed = m_maxMoveSpeed.Reset();

        public void SetVelocityPower(float power) => m_velocityPower.Value = power;
        public void ResetVelocityPower() => m_velocityPower.Reset();

        public void SetFriction(float amount, bool airborne = false) {
            if (airborne)
                m_frictionAirborne.Value = amount;
            else
                m_frictionGrounded.Value = amount;
        }

        public void ResetFriction(bool airborne = false) {
            if (airborne)
                m_frictionAirborne.Reset();
            else
                m_frictionGrounded.Reset();
        }
        #endregion

        #region Private Methods
        private void KeepOnGround() {
            var origin = m_collider.ClosestPoint(GroundPoint);
            var dir = GroundPoint - origin;
            if (dir.magnitude > .001f)
                Body.position += dir;

            if (Velocity.y < 0)
                return;

            Vector2 normalVelocity = Vector3.Project(Velocity, GroundNormal);
            Velocity -= normalVelocity;
        }
        #endregion

        #region Event Methods
        #endregion
    }
}