using CharacterControllers.Runtime.Utility;
using JetBrains.Annotations;
using UnityEngine;

namespace CharacterControllers.Runtime.Controllers {
    [System.Flags]
    public enum PhysicalAxis { None = 0, X = 1, Y = 2, Z = 4, XY = 3, XZ = 5, YZ = 6, All = 7 }

    [AddComponentMenu("Character Controllers/Controllers/Kinematic Controller")]
    [Icon("Packages/com.itstheconnection.character-controllers/Editor Default Resources/Script Icons/KinematicController - Icon.png")]
    [RequireComponent(typeof(Rigidbody))]
    [PublicAPI]
    public sealed class KinematicController : MonoBehaviour {
        #region Public Variables
        [Header("Settings")]
        [SerializeField] private DefaultedValue<float> m_gravityWeight = 1f;
        [SerializeField] private DefaultedValue<float> m_drag = 0f;

        [Space]
        [SerializeField] private bool m_alwaysApplyGravity;

        [Header("Limits")]
        [SerializeField] private DefaultedValue<float> m_maxHorizontalVelocity = 20f;
        [SerializeField] private DefaultedValue<float> m_maxUpwardsVelocity = 24f;
        [SerializeField] private DefaultedValue<float> m_maxDownwardsVelocity = 16f;
        #endregion

        #region Private Variables
        private Rigidbody _body;

        private Vector3? _motion;
        private Vector3? _gravityVelocity;
        private PhysicalAxis _dragAxis;
        private bool _dragLocal;
        #endregion

        #region Properties
        public Vector3 Velocity { get; private set; }
        #endregion

        #region Behaviour Callbacks
        private void Awake() {
            _body = GetComponent<Rigidbody>();
        }

        private void FixedUpdate() {
            if (m_alwaysApplyGravity && !_gravityVelocity.HasValue)
                ApplyGravity();

            var velocity = Velocity;

            // Applying user motion if desired
            if (_motion.HasValue) {
                velocity += _motion.Value;
                _motion = null;
            }

            // Applying gravity if desired
            if (_gravityVelocity.HasValue) {
                velocity += _gravityVelocity.Value;
                _gravityVelocity = null;
            }

            // Applying drag if desired
            if (_dragAxis != PhysicalAxis.None) {
                velocity = CalculateVelocityAfterDrag(velocity);
                _dragAxis = PhysicalAxis.None;
            }

            // Clamping velocity
            var clampedXz = Vector2.ClampMagnitude(new(velocity.x, velocity.z), m_maxHorizontalVelocity);
            var clampedY = Mathf.Clamp(velocity.y, -m_maxDownwardsVelocity, m_maxUpwardsVelocity);
            velocity = new(clampedXz.x, clampedY, clampedXz.y);

            var targetPosition = _body.position + velocity * Time.fixedDeltaTime;
            var safeDelta = PhysicsUtils.SafeCastToPosition(_body, targetPosition);
            var deltaDelta = (targetPosition - _body.position) - safeDelta;
            velocity -= deltaDelta;
            Debug.DrawRay(_body.position, safeDelta, Color.cyan, Time.fixedDeltaTime);

            // Moving
            _body.MovePosition(targetPosition + safeDelta);

            Velocity = velocity;
        }
        #endregion

        #region Public Methods
        public void ApplyDrag(PhysicalAxis axis, bool locally = false) {
            _dragAxis = axis;
            _dragLocal = locally;
        }

        public void ApplyGravity() {
            var accel = Physics.gravity * m_gravityWeight;
            var velocity = accel * Time.fixedDeltaTime;

            if (_gravityVelocity.HasValue)
                _gravityVelocity += velocity;
            else
                _gravityVelocity = velocity;
        }

        public void Move(Vector3 velocity) {
            if (_motion.HasValue)
                _motion += velocity;
            else
                _motion = velocity;
        }
        #endregion

        #region Public Setters
        public void SetMaxHorizontalSpeed(float maxSpeed) => m_maxHorizontalVelocity.Set(maxSpeed);
        public void ResetMaxHorizontalSpeed() => m_maxHorizontalVelocity.Reset();

        public void SetMaxUpwardsSpeed(float maxSpeed) => m_maxUpwardsVelocity.Set(maxSpeed);
        public void ResetMaxUpwardsSpeed() => m_maxUpwardsVelocity.Reset();

        public void SetMaxDownwardsSpeed(float maxSpeed) => m_maxDownwardsVelocity.Set(maxSpeed);
        public void ResetMaxDownwardsSpeed() => m_maxDownwardsVelocity.Reset();
        #endregion

        #region Private Methods
        private Vector3 CalculateVelocityAfterDrag(Vector3 velocity) {
            var vel = Vector3.zero;

            if (_dragAxis.HasFlag(PhysicalAxis.X))
                vel.x = velocity.x;
            if (_dragAxis.HasFlag(PhysicalAxis.Y))
                vel.y = velocity.y;
            if (_dragAxis.HasFlag(PhysicalAxis.Z))
                vel.z = velocity.z;

            var resultVel = PhysicsUtils.ApplyDrag(vel, m_drag);

            if (!_dragAxis.HasFlag(PhysicalAxis.X))
                resultVel.x = velocity.x;
            if (!_dragAxis.HasFlag(PhysicalAxis.Y))
                resultVel.y = velocity.y;
            if (!_dragAxis.HasFlag(PhysicalAxis.Z))
                resultVel.z = velocity.z;

            return resultVel;
        }
        #endregion

        #region Event Methods
        #endregion
    }
}