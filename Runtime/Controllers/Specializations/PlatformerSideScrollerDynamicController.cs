using CharacterControllers.Runtime.PhysicsAwareness;
using CharacterControllers.Runtime.Utility;
using JetBrains.Annotations;
using UnityEngine;

namespace CharacterControllers.Runtime.Controllers.Specializations {
    [AddComponentMenu("Character Controllers/Dynamics/Platformer SideScroller Controller")]
    [PublicAPI]
    public sealed class PlatformerSideScrollerDynamicController : DynamicController {
        #region Public Variables
        [SerializeField, Range(0, 2)] private float m_velocityPower = 1f;
        [SerializeField] private float m_inertialDrag = 2f;

        [Space]
        [SerializeField] private DefaultedValue<float> m_moveSpeed = 6f;
        [SerializeField] private DefaultedValue<float> m_acceleration = 2f;
        [SerializeField] private DefaultedValue<float> m_deceleration = 6f;

        [field: Space]
        [field: SerializeField] public bool UseFriction { get; set; } = true;
        [SerializeField] private DefaultedValue<float> m_friction = 1f;
        #endregion

        #region Private Variables
        private float _moveSpeed;
        private float _acceleration, _deceleration;
        private float _friction;

        private float _coyoteTimeEnd;

        private Vector3 _inertia;
        private Vector3 _groundTangent;

        private GroundAwareness _ground;
        #endregion

        #region Properties
        /// <summary>
        /// Input used to determine how to add motion forces.
        /// </summary>
        /// <remarks>Set it through <see cref="SetMotion"/>.</remarks>
        public Vector2 Direction { get; private set; }
        /// <summary>
        /// Defines the direction where the ground surface is facing.
        /// </summary>
        /// <remarks>Set it through <see cref="SetMotion"/>.</remarks>
        public Vector3 GroundNormal { get; private set; }
        #endregion

        #region Behaviour Callbacks
        private void OnValidate() {
            if (!GetComponent<GroundAwareness>())
                Debug.LogWarning("'Platformer SideScroller Controller' component would work best with a 'Ground Awareness' attached.\n", this);
        }

        private void OnDrawGizmos() {
            Gizmos.DrawRay(transform.position, GroundNormal);
        }
        #endregion

        #region Overrides
        protected override void OnInitialize() => TryGetComponent(out _ground);

        protected override void OnUpdate() {
            _moveSpeed = Mathf.Lerp(_moveSpeed, m_moveSpeed, Time.deltaTime * m_valuesLerpQuickness);

            _acceleration = Mathf.Lerp(_acceleration, m_acceleration, Time.deltaTime * m_valuesLerpQuickness);
            _deceleration = Mathf.Lerp(_deceleration, m_deceleration, Time.deltaTime * m_valuesLerpQuickness);
            _friction = Mathf.Lerp(_friction, m_friction, Time.deltaTime * m_valuesLerpQuickness);
        }

        protected override void OnHandleMovement() {
            _inertia = _inertia.sqrMagnitude > .01f ? InertialVelocityDrag(_inertia) : Vector3.zero;
            _groundTangent = Quaternion.Euler(0, 0, -90f) * GroundNormal;

            // Calculating motion force
            var targetSpeedX = Direction.x * _moveSpeed + _inertia.x; // Target speed includes the current inertia
            var speedDifX = targetSpeedX - Body.linearVelocity.x;
            var accelRate = Mathf.Abs(targetSpeedX) > .01f ? _acceleration : _deceleration;
            var movementX = Mathf.Pow(Mathf.Abs(speedDifX) * accelRate, m_velocityPower) * Mathf.Sign(speedDifX);
            Body.AddForce(_groundTangent * movementX);

            // Applying inertia
            if (_inertia.sqrMagnitude > .01f)
                Body.AddForce(Mathf.Pow(_inertia.magnitude, 2) * Mathf.Sign(_inertia.x) * _groundTangent, ForceMode.Acceleration);

            // Applying friction
            if (UseFriction && Mathf.Abs(Direction.x) < .01f)
                ApplyFriction();
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Sets the desired move speed.
        /// </summary>
        /// <param name="moveSpeed">The speed to reach when moving sideways.</param>
        /// <param name="sudden">Whether to change the value instantly or smoothly.</param>
        public void SetMoveSpeed(float moveSpeed, bool sudden = false) {
            m_moveSpeed.Set(moveSpeed);
            if (sudden) _moveSpeed = m_moveSpeed;
        }
        /// <summary>
        /// Reset the move speed to its original value.
        /// </summary>
        /// <param name="sudden">Whether to change the value instantly or smoothly.</param>
        public void ResetMoveSpeed(bool sudden = false) {
            m_moveSpeed.Reset();
            if (sudden) _moveSpeed = m_moveSpeed;
        }

        /// <summary>
        /// Sets <i>how quickly the target move speed is reached</i>.
        /// </summary>
        /// <param name="acceleration">The acceleration to use in order to reach the desired move speed.</param>
        /// <param name="sudden">Whether to change the value instantly or smoothly.</param>
        public void SetAcceleration(float acceleration, bool sudden = false) {
            m_acceleration.Set(acceleration);
            if (sudden) _acceleration = m_acceleration;
        }
        /// <summary>
        /// Resets <i>how quickly the target move speed is reached</i> to its origin value.
        /// </summary>
        /// <param name="sudden">Whether to change the value instantly or smoothly.</param>
        public void ResetAcceleration(bool sudden = false) {
            m_acceleration.Reset();
            if (sudden) _acceleration = m_acceleration;
        }

        /// <summary>
        /// Sets how quickly the rigid body slows down.
        /// </summary>
        /// <param name="deceleration">The deceleration to use in order to reach stillness.</param>
        /// <param name="sudden">Whether to change the value instantly or smoothly.</param>
        public void SetDeceleration(float deceleration, bool sudden = false) {
            m_deceleration.Set(deceleration);
            if (sudden) _deceleration = m_deceleration;
        }
        /// <summary>
        /// Resets rigid body slowdown quickness to its original value.
        /// </summary>
        /// <param name="sudden">Whether to change the value instantly or smoothly.</param>
        public void ResetDeceleration(bool sudden = false) {
            m_deceleration.Reset();
            if (sudden) _deceleration = m_deceleration;
        }

        /// <summary>
        /// Sets the friction multiplier for slowing down when <see cref="Direction"/> is zero.
        /// </summary>
        /// <param name="friction"></param>
        /// <param name="sudden">Whether to change the value instantly or smoothly.</param>
        public void SetFriction(float friction, bool sudden = false) {
            m_friction.Set(friction);
            if (sudden) _friction = m_friction;
        }
        /// <summary>
        /// Resets the friction multiplier for slowing down when <see cref="Direction"/> is zero, to its original value.
        /// </summary>
        /// <param name="sudden">Whether to change the value instantly or smoothly.</param>
        public void ResetFriction(bool sudden = false) {
            m_friction.Reset();
            if (sudden) _friction = m_friction;
        }

        /// <summary>
        /// Adds velocity to the current inertia.
        /// </summary>
        /// <param name="velocity">The amount of inertia to add.</param>
        public void AddInertialVelocity(Vector3 velocity) => _inertia += velocity;
        /// <summary>
        /// Clears the current inertia and sets it to the given velocity.
        /// </summary>
        /// <param name="velocity">The amount of inertia to set.</param>
        public void SetInertialVelocity(Vector3 velocity) => _inertia = velocity;
        /// <summary>
        /// Clears the current inertia.
        /// </summary>
        public void ClearInertialVelocity() => _inertia = Vector3.zero;

        /// <summary>
        /// Sets the movement input and conditions for the movement to be applied.
        /// </summary>
        /// <param name="direction">The direction of the desired movement.</param>
        /// <param name="groundNormal">The ground's surface facing direction. Used to adjust motion vectors for slopes.</param>
        public void SetMotion(Vector2 direction, Vector3 groundNormal = default) {
            Direction = direction;
            GroundNormal = groundNormal == default
                ? Vector3.up
                : groundNormal;
        }

        public void ResetMotion() => Direction = Vector2.zero;
        public void ResetGround() => GroundNormal = Vector3.up;
        #endregion

        #region Private Methods
        private Vector3 InertialVelocityDrag(Vector3 velocity) {
            velocity = PhysicsUtils.ApplyDragSmart(velocity, m_inertialDrag);
            Debug.DrawRay(Body.position, velocity, Color.cyan, Time.fixedDeltaTime);
            return velocity;
        }

        private void ApplyFriction() {
            var localVar = Vector3.ProjectOnPlane(Body.linearVelocity, GroundNormal);
            var amount = Mathf.Min(Mathf.Abs(localVar.x), _friction * .01f);
            amount *= Mathf.Sign(localVar.x);
            Body.AddForce(_groundTangent * -amount, ForceMode.Impulse);
        }
        #endregion
    }
}