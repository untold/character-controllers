using System.Collections.Generic;
using CharacterControllers.Runtime.Components.Platformer;
using CharacterControllers.Runtime.Utility;
using JetBrains.Annotations;
using UnityEngine;

namespace CharacterControllers.Runtime.Controllers.Specializations {
    [AddComponentMenu("Character Controllers/Dynamics/Platformer SideScroller Depth Handler")]
    [PublicAPI]
    public sealed class PlatformerSideScrollerDepthHandler : MonoBehaviour, IDepthController {
        #region Public Variables
        [SerializeField] private DefaultedValue<float> m_depth = 0;
        [SerializeField, Min(0.1f)] private float m_depthLerpQuickness = 12f;
        #endregion

        #region Private Variables
        private readonly List<PlatformerDepthTriggerBase> _depthTriggers = new();
        #endregion

        #region Properties
        public Rigidbody Body { get; private set; }

        public float BaseDepth => m_depth;
        #endregion

        #region Behaviour Callbacks
        private void OnValidate() {
            Body = GetComponent<Rigidbody>();
            // ReSharper disable once BitwiseOperatorOnEnumWithoutFlags
            if ((Body.constraints & RigidbodyConstraints.FreezePositionZ) != 0)
                Body.constraints ^= RigidbodyConstraints.FreezePositionZ;
        }

        private void Awake() => Body = GetComponent<Rigidbody>();

        private void FixedUpdate() => ControlDepth();
        #endregion

        #region Public Methods
        public void AddDepthTrigger(PlatformerDepthTriggerBase trigger) {
            _depthTriggers.Add(trigger);
            _depthTriggers.Sort();
        }

        public void RemoveDepthTrigger(PlatformerDepthTriggerBase trigger) => _depthTriggers.Remove(trigger);

        public void SetBaseDepth(float depth) => m_depth.Set(depth);
        public void ResetBaseDepth() => m_depth.Reset();
        #endregion

        #region Private Methods
        private void ControlDepth() {
            var vel = Body.linearVelocity;
            vel.z = 0;
            Body.linearVelocity = vel;

            var pos = Body.position;
            pos.z = BaseDepth;

            var t = Time.deltaTime * m_depthLerpQuickness;

            foreach (var trigger in _depthTriggers) {
                switch (trigger.Mode) {
                    case DepthMode.Set:
                        pos.z = trigger.GetDepthAt(pos);
                        Body.MovePosition(Vector3.Lerp(Body.position, pos, t));
                        return;
                    case DepthMode.Add:
                        pos.z += trigger.GetDepthAt(pos);
                        break;
                    case DepthMode.Subtract:
                        pos.z -= trigger.GetDepthAt(pos);
                        break;
                    default:
                        continue;
                }
            }

            Body.MovePosition(Vector3.Lerp(Body.position, pos, t));
        }
        #endregion
    }
}