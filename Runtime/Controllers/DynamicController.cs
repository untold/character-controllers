using CharacterControllers.Runtime.Utility;
using JetBrains.Annotations;
using UnityEngine;

namespace CharacterControllers.Runtime.Controllers {
    [AddComponentMenu("Character Controllers/Controllers/Dynamic Controller")]
    [Icon("Packages/com.itstheconnection.character-controllers/Editor Default Resources/Script Icons/PhysicsController - Icon.png")]
    [RequireComponent(typeof(Rigidbody))]
    [DisallowMultipleComponent]
    [PublicAPI]
    public class DynamicController : MonoBehaviour {
        #region Public Variables
        [SerializeField] private DefaultedValue<float> m_gravityWeight = 1f;
        [SerializeField] private DefaultedValue<float> m_drag = 0f;

        [SerializeField] private DefaultedValue<float> m_maxHorizontalVelocity = 20f;
        [SerializeField] private DefaultedValue<float> m_maxUpwardsVelocity = 24f;
        [SerializeField] private DefaultedValue<float> m_maxDownwardsVelocity = 16f;

        [SerializeField] private bool m_alwaysApplyGravity;
        [SerializeField] private float m_keepOnGroundCastDistance = .1f;
        [SerializeField, Min(0.01f)] protected float m_valuesLerpQuickness = 2f;
        #endregion

        #region Private Variables
        private PhysicalAxis _dragAxis;

        private float _dragCurrent;
        private float _gravityWeightCurrent;
        private float _maxDownwardsVelocityCurrent;
        #endregion

        #region Properties
        public Rigidbody Body { get; private set; }

        public Vector3 Velocity => Body.linearVelocity;
        #endregion

        #region Behaviour Callbacks
        private void Awake() {
            Body = GetComponent<Rigidbody>();
        }

        private void Start() => OnInitialize();

        private void Update() {
            _dragCurrent = Mathf.Lerp(_dragCurrent, m_drag, Time.deltaTime * m_valuesLerpQuickness);
            _gravityWeightCurrent = Mathf.Lerp(_gravityWeightCurrent, m_gravityWeight, Time.deltaTime * m_valuesLerpQuickness);
            _maxDownwardsVelocityCurrent = Mathf.Lerp(_maxDownwardsVelocityCurrent, m_maxDownwardsVelocity, Time.deltaTime * m_valuesLerpQuickness);

            OnUpdate();
        }

        private void FixedUpdate() {
            if (m_alwaysApplyGravity)
                ApplyGravity();

            OnHandleMovement();

            var velocity = Body.linearVelocity;

            // Applying drag if desired
            if (_dragAxis != PhysicalAxis.None) {
                velocity = CalculateVelocityAfterDrag(velocity);
                _dragAxis = PhysicalAxis.None;
            }

            // Clamping velocity
            var clampedXz = Vector2.ClampMagnitude(new(velocity.x, velocity.z), m_maxHorizontalVelocity);
            var clampedY = Mathf.Clamp(velocity.y, -m_maxDownwardsVelocity, m_maxUpwardsVelocity);
            velocity = new(clampedXz.x, clampedY, clampedXz.y);

            // Moving
            Body.linearVelocity = velocity;
        }
        #endregion

        #region Public Methods
        public void SetGravityWeight(float weight, bool sudden = false) {
            m_gravityWeight.Set(weight);
            if (sudden)
                _gravityWeightCurrent = m_gravityWeight;
        }
        public void ResetGravityWeight(bool sudden = false) {
            m_gravityWeight.Reset();
            if (sudden)
                _gravityWeightCurrent = m_gravityWeight;
        }

        public void SetMaxFallSpeed(float speed, bool sudden = false) {
            m_maxDownwardsVelocity.Set(speed);
            if (sudden)
                _maxDownwardsVelocityCurrent = m_maxDownwardsVelocity;
        }
        public void ResetMaxFallSpeed(bool sudden = false) {
            m_maxDownwardsVelocity.Reset();
            if (sudden)
                _maxDownwardsVelocityCurrent = m_maxDownwardsVelocity;
        }

        /// <summary>
        /// Applies a drag force to the rigid body on the given <see cref="PhysicalAxis"/>.
        /// </summary>
        /// <param name="axis">The axis on which to apply the drag.</param>
        public void ApplyDrag(PhysicalAxis axis) => _dragAxis |= axis;

        /// <summary>
        /// Adds the <see cref="Physics.gravity"/> to the rigid body, multiplied by a <b>weight</b>.
        /// </summary>
        public void ApplyGravity() {
            if (Mathf.Abs(_gravityWeightCurrent) < .01f)
                return;

            var velocity = Body.linearVelocity;
            if (velocity.y > -m_maxDownwardsVelocity)
                Body.AddForce(Physics.gravity * _gravityWeightCurrent, ForceMode.Acceleration);
            else if (velocity.y < -m_maxDownwardsVelocity - 0.5f)
                Body.AddForce(-Physics.gravity.normalized * m_drag, ForceMode.Acceleration);
        }
        /// <summary>
        /// Adds the <see cref="Physics.gravity"/> perpendicular to the ground's surface to the rigid body, multiplied by a <b>weight</b>.
        /// </summary>
        public void ApplyGravity(Vector3 groundNormal) {
            if (Mathf.Abs(_gravityWeightCurrent) < .01f)
                return;

            var normalGravity = Vector3.Project(Physics.gravity, groundNormal);
            var velocity = Body.linearVelocity;
            if (velocity.y > -m_maxDownwardsVelocity)
                Body.AddForce(normalGravity * _gravityWeightCurrent, ForceMode.Acceleration);
            else if (velocity.y < -m_maxDownwardsVelocity - 0.5f)
                Body.AddForce(-normalGravity.normalized * m_drag, ForceMode.Acceleration);
        }

        /// <summary>
        /// Moves the rigid body by applying a <see cref="ForceMode.VelocityChange"/> type of force.
        /// </summary>
        /// <param name="velocity">The velocity to apply.</param>
        public void Move(Vector3 velocity) => Body.AddForce(velocity, ForceMode.VelocityChange);

        /// <summary>
        /// Moves the rigid body by applying a <see cref="ForceMode.Force"/> type of force.
        /// </summary>
        /// <param name="force">The force to apply.</param>
        public void Push(Vector3 force) => Body.AddForce(force);

        /// <summary>
        /// Moves the rigid body by applying a <see cref="ForceMode.Impulse"/> type of force.
        /// </summary>
        /// <param name="force">The impulse to apply.</param>
        public void Kick(Vector3 force) => Body.AddForce(force, ForceMode.Impulse);

        /// <summary>
        /// Moves the rigid body by applying a <see cref="ForceMode.Acceleration"/> type of force.
        /// </summary>
        /// <param name="acceleration">The acceleration to apply.</param>
        public void Accelerate(Vector3 acceleration) => Body.AddForce(acceleration, ForceMode.Acceleration);

        /// <summary>
        /// Zeroes out the rigid body's velocity on the given <see cref="PhysicalAxis"/>.
        /// </summary>
        /// <param name="axis">The axis to zero out.</param>
        public void Freeze(PhysicalAxis axis = PhysicalAxis.All) {
            var vel = Body.linearVelocity;

            if (axis.HasFlag(PhysicalAxis.X))
                vel.x = 0f;
            if (axis.HasFlag(PhysicalAxis.Y))
                vel.y = 0f;
            if (axis.HasFlag(PhysicalAxis.Z))
                vel.z = 0f;

            Body.linearVelocity = vel;
        }

        /// <summary>
        /// Sets the rigid body's velocity, except for the given <see cref="PhysicalAxis"/>.
        /// </summary>
        /// <param name="velocity">The velocity to set.</param>
        /// <param name="exceptAxis">The axis that should keep their current values.</param>
        public void SetVelocity(Vector3 velocity, PhysicalAxis exceptAxis = PhysicalAxis.None) {
            if (exceptAxis.HasFlag(PhysicalAxis.X))
                velocity.x = Body.linearVelocity.x;
            if (exceptAxis.HasFlag(PhysicalAxis.Y))
                velocity.y = Body.linearVelocity.y;
            if (exceptAxis.HasFlag(PhysicalAxis.Z))
                velocity.z = Body.linearVelocity.z;

            Body.linearVelocity = velocity;
        }

        public void SetPosition(Vector3 position, PhysicalAxis exceptAxis = PhysicalAxis.None) {
            if (exceptAxis.HasFlag(PhysicalAxis.X))
                position.x = Body.position.x;
            if (exceptAxis.HasFlag(PhysicalAxis.Y))
                position.y = Body.position.y;
            if (exceptAxis.HasFlag(PhysicalAxis.Z))
                position.z = Body.position.z;

            Body.position = position;
        }

        /// <summary>
        /// Given the <b>normal</b> of the ground surface, tries to move the rigid body towards the ground.
        /// </summary>
        /// <param name="groundNormal">The ground's normal.</param>
        /// <param name="angleThreshold">The angle over which the ground is considered slope.</param>
        public void KeepOnGround(Vector3 groundNormal, float angleThreshold = 90f) {
            var dir = -groundNormal;
            dir.Normalize();

            for (var i = 0; i < 10; i++) {
                var didHitGround = Body.SweepTest(dir, out var hit, m_keepOnGroundCastDistance, QueryTriggerInteraction.Ignore);

                if (Vector3.Angle(Vector3.up, groundNormal) > angleThreshold)
                    return;

                if (!didHitGround)
                    return;

                var ray = new Ray {
                    origin = Body.position + transform.up * .1f,
                    direction = -transform.up
                };
                if (Physics.Raycast(ray, out var rayHit, 0.2f))
                    Body.linearVelocity = Vector3.ProjectOnPlane(Body.linearVelocity, rayHit.normal);
                Body.linearVelocity += dir * hit.distance;

                // Body.linearVelocity = Vector3.ProjectOnPlane(Body.linearVelocity, groundNormal);
                Body.position += (hit.distance - Physics.defaultContactOffset) * Time.fixedDeltaTime * dir;
            }
        }

        /// <summary>
        /// Given a moving platform and a cast direction, it pushes the rigid body towards it while also
        /// offsetting it using the <see cref="Rigidbody.GetRelativePointVelocity(Vector3)"/>.
        /// </summary>
        /// <param name="other">The moving platform to follow.</param>
        /// <param name="castDirection">The direction to cast to.<br/>Should point towards the platform.</param>
        public void AnchorToOtherBody(Rigidbody other, Vector3 castDirection) {
            var pointVelocity = other.GetRelativePointVelocity(Body.position);
            Body.MovePosition(Body.position + pointVelocity * Time.fixedDeltaTime);

            castDirection.Normalize();
            if (Body.SweepTest(castDirection, out var hit, pointVelocity.magnitude * Time.fixedDeltaTime, QueryTriggerInteraction.Ignore))
                Body.position -= castDirection * (hit.distance - Physics.defaultContactOffset);
        }
        #endregion

        #region Public Setters
        public void SetMaxHorizontalSpeed(float maxSpeed) => m_maxHorizontalVelocity.Set(maxSpeed);
        public void ResetMaxHorizontalSpeed() => m_maxHorizontalVelocity.Reset();

        public void SetMaxUpwardsSpeed(float maxSpeed) => m_maxUpwardsVelocity.Set(maxSpeed);
        public void ResetMaxUpwardsSpeed() => m_maxUpwardsVelocity.Reset();

        public void SetMaxDownwardsSpeed(float maxSpeed) => m_maxDownwardsVelocity.Set(maxSpeed);
        public void ResetMaxDownwardsSpeed() => m_maxDownwardsVelocity.Reset();

        public void SetDrag(float drag, bool smooth = false) {
            m_drag.Set(drag);
            if (!smooth)
                _dragCurrent = m_drag;
        }

        public void ResetDrag(bool smooth = false) {
            m_drag.Reset();
            if (!smooth)
                _dragCurrent = m_drag;
        }
        #endregion

        #region Private Methods
        private Vector3 CalculateVelocityAfterDrag(Vector3 velocity) {
            var vel = Vector3.zero;

            if (_dragAxis.HasFlag(PhysicalAxis.X))
                vel.x = velocity.x;
            if (_dragAxis.HasFlag(PhysicalAxis.Y))
                vel.y = velocity.y;
            if (_dragAxis.HasFlag(PhysicalAxis.Z))
                vel.z = velocity.z;

            var resultVel = PhysicsUtils.ApplyDrag(vel, _dragCurrent);

            if (!_dragAxis.HasFlag(PhysicalAxis.X))
                resultVel.x = velocity.x;
            if (!_dragAxis.HasFlag(PhysicalAxis.Y))
                resultVel.y = velocity.y;
            if (!_dragAxis.HasFlag(PhysicalAxis.Z))
                resultVel.z = velocity.z;

            return resultVel;
        }

        protected virtual void OnInitialize() { }
        protected virtual void OnUpdate() {}
        protected virtual void OnHandleMovement() {}
        #endregion

        #region Event Methods
        #endregion
    }
}