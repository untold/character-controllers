using UnityEngine;

namespace CharacterControllers.Runtime.PhysicsAwareness {
    [AddComponentMenu("Character Controllers/Awareness/Walls/Walls SideScroller Line Caster")]
    public sealed class WallsSideScrollerLineCaster : WallsAwareness {
        #region Public Variables
        [SerializeField] private QueryTriggerInteraction m_trigger = QueryTriggerInteraction.Ignore;
        [SerializeField] private Vector3 m_castOffset = new(0.1f, 0, 0);
        [SerializeField] private float m_castDistance = 0.2f;
        [SerializeField] private bool m_useLocalX;
        #endregion

        #region Private Variables
        private RaycastHit _hit;
        #endregion

        #region Behaviour Callbacks
        private void OnValidate() {
            if (Physics.queriesHitBackfaces)
                Debug.LogWarning("'Queries Hit BackFaces' is enabled in the Physics Settings\n" +
                                 "Disable it to avoid unexpected casting behaviours.\n", this);
        }

        private void OnDrawGizmosSelected() {
            var isDirecting = Mathf.Abs(Direction.x) > .5f;
            var dir = Mathf.Sign(Direction.x);

            var offset = m_castOffset;
            if (isDirecting)
                offset.x *= dir;
            var ray = new Ray {
                origin = transform.TransformPoint(offset),
                direction = m_useLocalX ? transform.right : Vector3.right
            };
            if (isDirecting)
                ray.direction *= dir;

            Gizmos.DrawLine(ray.origin, ray.origin + ray.direction * m_castDistance);

            var hitPoint = ray.GetPoint(IsAware ? _hit.distance : m_castDistance);

            Gizmos.color = IsAware ? Color.green : Color.red;
            Gizmos.DrawLine(ray.origin, hitPoint);
            Gizmos.color = Color.white;

            if (isDirecting)
                return;

            offset = m_castOffset;
            offset.x *= -1;
            var rayL = new Ray {
                origin = transform.TransformPoint(offset),
                direction = m_useLocalX ? -transform.right : Vector3.left
            };

            Gizmos.DrawLine(rayL.origin, rayL.origin + rayL.direction * m_castDistance);

            var hitPointL = rayL.GetPoint(IsAware ? _hit.distance : m_castDistance);

            Gizmos.color = IsAware ? Color.green : Color.red;
            Gizmos.DrawLine(rayL.origin, hitPointL);
            Gizmos.color = Color.white;
        }
        #endregion

        #region Overrides
        protected override bool OnAwarenessCheck() {
            var isDirecting = Mathf.Abs(Direction.x) > .5f;
            var dir = Mathf.Sign(Direction.x);

            var offset = m_castOffset;
            if (isDirecting)
                offset.x *= dir;
            var ray = new Ray {
                origin = transform.TransformPoint(offset),
                direction = m_useLocalX ? transform.right : Vector3.right
            };
            if (isDirecting)
                ray.direction *= dir;

            if (Physics.Raycast(ray, out _hit, m_castDistance, WallLayers, m_trigger)) {
                Normal = _hit.normal;
                Angle = Vector3.Angle(_hit.normal, Vector3.left);
                Body = _hit.rigidbody;
                return true;
            }

            if (isDirecting)
                return false;

            offset = m_castOffset;
            offset.x *= -1;
            ray = new Ray {
                origin = transform.TransformPoint(offset),
                direction = m_useLocalX ? -transform.right : Vector3.left
            };

            if (!Physics.Raycast(ray, out _hit, m_castDistance, WallLayers, m_trigger))
                return false;

            Normal = _hit.normal;
            Angle = Vector3.Angle(_hit.normal, Vector3.right);
            Body = _hit.rigidbody;
            return true;
        }
        #endregion
    }
}