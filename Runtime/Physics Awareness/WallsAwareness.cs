using CharacterControllers.Runtime.Core;
using UnityEngine;

namespace CharacterControllers.Runtime.PhysicsAwareness {
    [DisallowMultipleComponent]
    public abstract class WallsAwareness : PhysicsAwarenessBase {
        #region Public Variables
        [field: SerializeField] protected LayerMask WallLayers { get; private set; } = ~Physics.DefaultRaycastLayers;
        #endregion

        #region Properties
        public Vector3 Direction { get; set; }

        public Vector3 Normal { get; set; }
        public float Angle { get; set; }

        public Rigidbody Body { get; set; }
        #endregion
    }
}