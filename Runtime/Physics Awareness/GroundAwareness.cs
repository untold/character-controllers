using CharacterControllers.Runtime.Core;
using UnityEngine;

namespace CharacterControllers.Runtime.PhysicsAwareness {
    [DisallowMultipleComponent]
    public abstract class GroundAwareness : PhysicsAwarenessBase {
        #region Public Variables
        [field: SerializeField] protected LayerMask GroundLayers { get; private set; } = ~Physics.DefaultRaycastLayers;
        [field: SerializeField, Range(0, 90)] public float SlopeAngle { get; private set; } = 35f;
        [field: SerializeField, Range(70, 90)] public float WallAngle { get; private set; } = 80f;
        #endregion

        #region Properties
        public Vector3 Normal { get; protected set; }
        public float Angle { get; protected set; }

        public Rigidbody Body { get; protected set; }
        #endregion

        #region Public Methods
        public bool IsOnSlope() => Angle > SlopeAngle && Angle < WallAngle;
        #endregion
    }
}