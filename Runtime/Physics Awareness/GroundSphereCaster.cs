using UnityEngine;

namespace CharacterControllers.Runtime.PhysicsAwareness {
    [AddComponentMenu("Character Controllers/Awareness/Ground/Ground Sphere Caster")]
    public sealed class GroundSphereCaster : GroundAwareness {
        #region Public Variables
        [SerializeField] private Vector3 m_castOffset = new(0, 0.1f, 0);
        [SerializeField, Min(0.001f)] private float m_castRadius = 0.125f;
        [SerializeField] private float m_castDistance = 0.2f;
        [SerializeField] private bool m_useLocalY = true;
        #endregion

        #region Private Variables
        private RaycastHit _hit;
        #endregion

        #region Behaviour Callbacks
        private void OnValidate() {
            if (Physics.queriesHitBackfaces)
                Debug.LogWarning("'Queries Hit BackFaces' is enabled in the Physics Settings\n" +
                                 "Disable it to avoid unexpected casting behaviours.\n", this);
        }

        private void OnDrawGizmosSelected() {
            var ray = new Ray {
                origin = transform.TransformPoint(m_castOffset),
                direction = m_useLocalY ? -transform.up : Vector3.down
            };

            Gizmos.DrawLine(ray.origin, ray.origin + ray.direction * m_castDistance);

            var hitPoint = ray.GetPoint(_hit.collider ? _hit.distance : m_castDistance);

            Gizmos.color = IsAware ? Color.green : Color.red;
            Gizmos.DrawLine(ray.origin, hitPoint);
            Gizmos.color *= .5f;
            Gizmos.DrawSphere(hitPoint, m_castRadius);
            Gizmos.color = Color.white;
        }
        #endregion

        #region Overrides
        protected override bool OnAwarenessCheck() {
            var ray = new Ray {
                origin = transform.TransformPoint(m_castOffset),
                direction = m_useLocalY ? -transform.up : Vector3.down
            };

            var didHit = Physics.SphereCast(ray, m_castRadius, out _hit, m_castDistance, GroundLayers, QueryTriggerInteraction.Ignore);

            Normal = didHit ? _hit.normal : Vector3.up;
            Angle = Vector3.Angle(Normal, Vector3.up);
            Body = _hit.rigidbody;

            return didHit;
        }
        #endregion
    }
}