using CharacterControllers.Runtime.Core;
using UnityEngine;

namespace CharacterControllers.Runtime.PhysicsAwareness {
    [AddComponentMenu("Character Controllers/Awareness/Edge Detector")]
    [DisallowMultipleComponent]
    public sealed class EdgeAwareness : PhysicsAwarenessBase {
        #region Public Variables
        [SerializeField] private LayerMask m_groundMask = 1;
        [SerializeField] private float m_castProjection = 0.3f;
        [SerializeField] private float m_offsetY = 0.3f;
        [SerializeField] private float m_castDepth = 0.5f;
        #endregion

        #region Properties
        public Vector3 Direction { get; set; }
        #endregion

        #region Behaviour Callbacks
        private void OnDrawGizmos() {
            var ray = new Ray {
                origin = transform.TransformPoint(Vector3.up * m_offsetY) + Direction * m_castProjection,
                direction = Vector3.down
            };

            Gizmos.color = IsAware ? Color.green : Color.red;
            Gizmos.DrawRay(ray.origin, ray.direction * m_castDepth);
            Gizmos.color = Color.white;
        }
        #endregion

        #region Overrides
        protected override bool OnAwarenessCheck() {
            var ray = new Ray {
                origin = transform.TransformPoint(Vector3.up * m_offsetY) + Direction * m_castProjection,
                direction = Vector3.down
            };

            return !Physics.Raycast(ray, m_castDepth, m_groundMask);
        }
        #endregion
    }
}