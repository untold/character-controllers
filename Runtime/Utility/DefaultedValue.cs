﻿using JetBrains.Annotations;
using UnityEngine;

namespace CharacterControllers.Runtime.Utility {
    [System.Serializable]
    [PublicAPI]
    public class DefaultedValue<T> where T : unmanaged {
        #region Public Variables
        [SerializeField] private T m_defaultValue;
        #endregion

        #region Properties
        [field: SerializeField] public T Value { get; set; }
        #endregion

        #region Constructors
        public DefaultedValue(T value) {
            m_defaultValue = value;
            Value = value;
        }
        #endregion

        #region Public Methods
        public void Set(T value) => Value = value;
        public T Get() => Value;
        public void Get(out T value) => value = Value;
        public T Reset() => Value = m_defaultValue;
        #endregion

        #region Operators
        public static implicit operator T(DefaultedValue<T> defVal) => defVal.Value;
        public static implicit operator DefaultedValue<T>(T val) => new(val);
        #endregion
    }
}