﻿using UnityEngine;

namespace CharacterControllers.Runtime.Utility {
    [System.Serializable]
    public sealed class PIDController {
        public enum DerivativeMethod {
            Delta,
            Velocity,
        }

        #region Public Variables
        [SerializeField] public DerivativeMethod m_method;
        [SerializeField] private float m_proportionalGain;
        // [SerializeField] private float m_integralGain;
        [SerializeField] private float m_derivativeGain;
        [Space]
        [SerializeField] private float m_maxMagnitude = 2f;
        #endregion

        #region Private Variables
        private bool _isInitialized;
        private Vector3 _lastDelta;
        private Vector3 _lastValue;
        #endregion

        #region Properties
        #endregion

        #region Constructors
        #endregion

        #region Public Methods
        public Vector3 Calculate(float dt, Vector3 currentValue, Vector3 targetValue) {
            var delta = targetValue - currentValue;

            var p = delta * m_proportionalGain;

            var d = Vector3.zero;

            // This is to avoid a spike during the first iteration
            if (_isInitialized) {
                var deltaRate = (delta - _lastDelta) / dt;
                _lastDelta = delta;

                var valueDelta = (currentValue - _lastValue) / dt;
                _lastValue = currentValue;

                d = m_method switch {
                    DerivativeMethod.Delta => deltaRate * m_derivativeGain,
                    DerivativeMethod.Velocity => -valueDelta * m_derivativeGain,
                    _ => deltaRate * m_derivativeGain,
                };
            } else
                _isInitialized = true;

            return Vector3.ClampMagnitude(p + d, m_maxMagnitude);
        }

        /// <summary>
        /// Invoke this if you manually change the target position
        /// </summary>
        public void Reset() => _isInitialized = false;
        #endregion

        #region Private Methods
        #endregion
    }
}