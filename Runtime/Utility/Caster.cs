using UnityEngine;

namespace CharacterControllers.Runtime.Utility {
    public static class Caster {
        #region Public Variables
        #endregion

        #region Private Variables
        #endregion

        #region Properties
        #endregion

        #region Behaviour Callbacks
        #endregion

        #region Public Methods
        public static bool CapsuleCast(CapsuleCollider capsule, Vector3 direction, float distance, out RaycastHit hit, float inset, LayerMask mask) {
            var t = capsule.transform;
            var topPoint = t.TransformPoint(capsule.center + (capsule.height * .5f - capsule.radius) * Vector3.up);
            var botPoint = t.TransformPoint(capsule.center + (capsule.height * .5f - capsule.radius) * Vector3.down);
            var radius = capsule.radius - inset * 2f;

            var hits = new RaycastHit[4];
            var size = Physics.CapsuleCastNonAlloc(topPoint, botPoint, radius, direction, hits, distance, mask, QueryTriggerInteraction.Ignore);

            if (size <= 0) {
                hit = default;
                return false;
            }

            hit = new RaycastHit {
                distance = Mathf.Infinity
            };
            for (var i = 0; i < size; i++) {
                var ithHit = hits[i];
                hit.normal += ithHit.normal;
                hit.point += ithHit.point;

                if (ithHit.distance >= hit.distance)
                    continue;

                hit.distance = ithHit.distance;
            }

            hit.distance -= inset * 2f;
            hit.point /= size;
            hit.normal.Normalize();
            return true;
        }
        #endregion

        #region Private Methods
        #endregion
    }
}