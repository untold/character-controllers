using UnityEngine;

namespace CharacterControllers.Runtime.Utility {
    public static class PhysicsUtils {
        #region Public Methods
        public static Vector3 ApplyDrag(Vector3 velocity, float drag) {
            var clampedDrag = Mathf.Max(0, drag);
            if (clampedDrag < 1)
                velocity *= (1 - clampedDrag);
            else
                velocity -= clampedDrag * Time.fixedDeltaTime * velocity;

            return velocity;
        }

        public static Vector3 ApplyDragSmart(Vector3 velocity, float drag) {
            var clampedDrag = Mathf.Max(0, drag);
            if (clampedDrag < 1)
                velocity *= (1 - clampedDrag) * Time.fixedDeltaTime;
            else
                velocity -= clampedDrag * Time.fixedDeltaTime * velocity;

            return velocity;
        }

        public static Vector2 ApplyDragSmart(Vector2 velocity, float drag) {
            var clampedDrag = Mathf.Max(0, drag);
            if (clampedDrag < 1)
                velocity *= (1 - clampedDrag) * Time.fixedDeltaTime;
            else
                velocity -= clampedDrag * Time.fixedDeltaTime * velocity;

            return velocity;
        }

        public static Vector3 SafeCastToPosition(Rigidbody rigidbody, Vector3 targetPosition) {
            var delta = targetPosition - rigidbody.position;
            var finalDelta = Vector3.zero;
            CastAndCorrectPosition(rigidbody, rigidbody.position, delta, ref finalDelta, true);
            return finalDelta;
        }
        #endregion

        #region Private Methods
        private static void CastAndCorrectPosition(Rigidbody rigidbody, Vector3 position, Vector3 moveDelta, ref Vector3 finalMoveDelta, bool firstIteration = false) {
            if (moveDelta.magnitude < .001f)
                return;

            var magnitude = moveDelta.magnitude;
            var ray = new Ray {
                origin = position,
                direction = moveDelta.normalized
            };

            var didHit = rigidbody.SweepTest(ray.direction, out var hit, magnitude, QueryTriggerInteraction.Ignore);
            Debug.Log($"DidHit: {didHit}\n");

            if (!didHit) {
                if (firstIteration)
                    finalMoveDelta = moveDelta;

                return;
            }

            var remainingMagnitude = magnitude - hit.distance;
            var tangent = Vector3.ProjectOnPlane(ray.direction, hit.normal).normalized;
            var actualDeltaMove = ray.direction * hit.distance + hit.normal * .01f;
            finalMoveDelta += actualDeltaMove;
            CastAndCorrectPosition(rigidbody, position + actualDeltaMove, tangent * remainingMagnitude, ref finalMoveDelta);
        }
        #endregion
    }
}